package family.dziedziczenie;

public class Daughter extends FamilyMember {

	public Daughter(String name) {
		super(name);
	}
	@Override
	public void introduce() {
		System.out.println("I'm a daughter, my name is : " + this.getName());
	}

}
