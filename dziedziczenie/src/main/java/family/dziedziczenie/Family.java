package family.dziedziczenie;

public class Family {

	public static void main(String[] args) {
		FamilyMember mother = new Mother("Gra�yna");
		// mother.introduce();
		//
		FamilyMember father = new Father("Andrzej");
		// father.introduce();
		//
		FamilyMember son = new Son("Adam");
		// son.introduce();
		//
		FamilyMember daughter = new Daughter("Ania");
		// daughter.introduce();

		FamilyMember[] members = { mother, son, father, daughter };

		for (FamilyMember familyMember : members) {
			familyMember.introduce();
		}

	}

}
