package instruments;

public class Main {

	public static void main(String[] args) {
		Instruments guitar = new Guitar(444, "czarnym");

		Instruments drum = new Drum(2300, "bia�ym");

		Instruments trumpet = new Trumpet(350, "z�otym");

		Instruments piano = new Piano(3200, "br�zowym");

		Instruments[] instruments = { guitar, trumpet, drum, piano };

		for (Instruments it : instruments) {
			it.showType();
			if (it instanceof InstrumentsInterface) {
				InstrumentsInterface ava = (InstrumentsInterface) it;
				InstrumentsInterface qu = (InstrumentsInterface) it;
				System.out.println("\tDost�pne: " + ava.available()+ ", ilo�� " + qu.quantity() );
			}
		}

	}

}
