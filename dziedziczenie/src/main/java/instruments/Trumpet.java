package instruments;

public class Trumpet extends Instruments implements InstrumentsInterface {

	public Trumpet(int cost, String color) {
		super("d�tym", cost, color);
	}

	@Override
	public void showType() {
		System.out.println("Tr�bka jest instrumentem " + this.getType() + " i kosztuje: " + this.getCost()
				+ "z�. Jest w kolorze " + getCol() + ".");
	}
	@Override
	public String available() {

		return "Nie";
	}
	@Override
	public int quantity(){
		return 0;
	}

}
