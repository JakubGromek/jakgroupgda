package instruments;

public class Guitar extends Instruments implements InstrumentsInterface {

	public Guitar(int cost, String color) {
		super("strunowym", cost, color);
	}

	@Override
	public void showType() {
		System.out.println("Gitara jest instrumentem " + this.getType() + " i kosztuje: " + this.getCost()
				+ "z�. Jest w kolorze " + getCol() + ".");
	}
	@Override
	public String available() {

		return "Tak";
	}
	@Override
	public int quantity(){
		return 4;
	}

}
