package instruments;

public class Drum extends Instruments implements InstrumentsInterface {

	public Drum(int cost, String color) {
		super("perkusyjnym", cost, color);
	}

	@Override
	public void showType() {
		System.out.println("B�bny s� instrumentem " + this.getType() + " i kosztuj�: " + this.getCost()
				+ "z�. Jest w kolorze " + getCol() + ".");
	}
	@Override
	public String available() {

		return "Nie";
	}
	@Override
	public int quantity() {
		return 0;
	}

}
