package instruments;

public class Piano extends Instruments implements InstrumentsInterface {

	public Piano(int cost, String color) {
		super("klawiszowym", cost, color);
	}

	@Override
	public void showType() {
		System.out.println("Pianino jest instrumentem " + this.getType() + " i kosztuje: " + this.getCost()
				+ "z�. Jest w kolorze " + getCol() + ".");
	}
	@Override
	public String available() {

		return "Tak";
	}
	@Override
	public int quantity(){
		return 4;
	}
	

}
