package instruments;

public interface InstrumentsInterface {

	public String available();
	public int quantity();

}
