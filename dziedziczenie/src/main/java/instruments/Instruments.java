package instruments;

public abstract class Instruments {
	
	private String type;
	private int cost;
	private String color;

	

	public Instruments(String type, int cost, String color) {
		this.type = type;
		this.cost = cost;
		this.color = color;
	}


	public String getType() {
		return type;
	}
	
	
	public int getCost() {
		return cost;
	}
	
	public String getCol() {
		return color;
	}

	public abstract void showType();

}
