package crud;

/**
 * Created by RENT on 2017-07-13.
 */
public interface CRUD<T> {
    void insertOrUpdate(T type);
    void delete(T type);
    void select(T type);
    void select();
}
