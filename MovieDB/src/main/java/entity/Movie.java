package entity;

import javax.persistence.*;

/**
 * Created by RENT on 2017-07-13.
 */
@Entity
@Table(name = "movieDB")
public class Movie {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "year")
    private int year;
    @Column(name = "duration")
    private double duration;
    @Column(name = "descreption")
    private String descreption;

//    @ManyToOne (fetch = FetchType.LAZY,cascade = CascadeType.ALL)
//    private String genre;

    public Movie(){}

    public Movie(String title, int year, double duration, String descreption) {
        this.title = title;
        this.year = year;
        this.duration = duration;
        this.descreption = descreption;
    }

//    public String getGenre() {
//        return genre;
//    }

//    public Movie setGenre(String genre) {
//        this.genre = genre;
//        return this;
//    }

    public int getId() {
        return id;
    }

    public Movie setId(int id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Movie setTitle(String title) {
        this.title = title;
        return this;
    }

    public int getYear() {
        return year;
    }

    public Movie setYear(int year) {
        this.year = year;
        return this;
    }

    public double getDuration() {
        return duration;
    }

    public Movie setDuration(double duration) {
        this.duration = duration;
        return this;
    }

    public String getDescreption() {
        return descreption;
    }

    public Movie setDescreption(String descreption) {
        this.descreption = descreption;
        return this;
    }
}
