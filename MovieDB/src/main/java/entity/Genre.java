package entity;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-07-13.
 */
@Entity
@Table(name = "genre")
public class Genre {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private int id;
    @Column(name = "name")
    private String name;

//    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    private List<Movie> movies = new LinkedList<Movie>();

//    public List<Movie> getMovies() {
//        return movies;
//    }
//
//    public Genre setMovies(List<Movie> movies) {
//        this.movies = movies;
//        return this;
//    }

    public Genre(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public Genre setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Genre setName(String name) {
        this.name = name;
        return this;
    }
}
