package pl.org.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MatrixPrint2dArrayTest {

	Matrix m;
	
	@Before
	public void init() {
		m = new Matrix();
	}
	
	@Test
	public void test() {
		int[][] actual = new int[3][3];
		int[][] expected = {{1,2,3},{4,5,6},{7,8,9}};
		assertArrayEquals(expected, m.print2DArray(actual));
	}



}
