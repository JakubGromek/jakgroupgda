package pl.org.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class IndexedMatrixTest {

	Matrix m;
	
	@Before
	public void init() {
		m = new Matrix();
	}
	
	@Test
	public void test() {
		int[][] actual = new int[3][3];
		int[][] expected = {{1,2,3},{4,5,6},{7,8,9}};
		assertArrayEquals(expected, m.indexedMatrix(actual));
	}
	
	@Test
	public void checkSecondArray() {
		int[][] actual = new int[3][2];
		int[][] expected = {{1,2},{3,4},{5,6}};
		assertArrayEquals(expected, m.indexedMatrix(actual)); 
	}
	
	



}
