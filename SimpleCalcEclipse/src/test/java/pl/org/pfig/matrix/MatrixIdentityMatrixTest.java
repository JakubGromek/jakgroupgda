package pl.org.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MatrixIdentityMatrixTest {

	Matrix m;
	
	@Before
	public void init() {
		m = new Matrix();
	}
	
	@Test
	public void test() {
		int[][] actual = new int[3][3];
		int[][] expected = {{1,0,0},{0,1,0},{0,0,1}};
		assertArrayEquals(expected, m.identityMatrix(actual));
	}
	
	@Test
	public void whenEmptyArrayGivenIdentityMatrixExpectedByValue(){
		int[][] actual = new int[3][3];
		int[][] expected = {{1,0,0},{0,1,0},{0,0,1}};
		actual = m.identityMatrix(actual);
		
		for(int i = 0;i<actual.length;i++){
			for(int j=0; j<actual.length; j++){
				assertEquals(expected[i][j], actual[i][j]);
			}
		}
	}
	
	public void whenEmptyArrayGivenCheckDImensionsOfArray () {
		int[][] actual = new int[3][3];
		int[][] expected = {{1,0,0},{0,1,0},{0,0,1}};
		assertTrue(actual.length == expected.length);
		assertFalse(actual.length != expected.length);
	}
	
	
	
	

}
