package pl.org.pfig.matrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AddMatrixTest {
	
	Matrix m;
	
	@Before
	public void init() {
		m = new Matrix();
	}

	@Test
	public void checkArraySumTest() {
		int[][] a = {{1,2},{3,4}};
		int[][] b = {{5,6},{7,8}};
		
		int[][] exp = {{6,8},{10,12}};
		
		assertArrayEquals(exp, m.addMatrix(a, b));
		
		
	}
	
	@Test(expected=Exception.class)
	public void whenExThrowMethodIsUsedExceptionIsThrown() throws Exception {
		
		
	}
}
