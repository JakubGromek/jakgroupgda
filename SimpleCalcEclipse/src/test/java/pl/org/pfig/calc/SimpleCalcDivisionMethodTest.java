package pl.org.pfig.calc;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SimpleCalcDivisionMethodTest {
	private SimpleCalc sc;
	
	@Before
	public void doStuff(){
		sc = new SimpleCalc();
	}

	@Test
	public void oneNegativeOnePositiveDivision() {
		int a = -4, b = 2;
		assertEquals(-2.0, sc.divided(a, b), 0.01);
	}
	
	@Test
	public void onePositiveOnePositiveDivision() {
		int a = 4, b = 2;
		assertEquals(2, sc.divided(a, b), 0.01);
	}
	
	@Test
	public void oneNegativeOneNegativeDivision() {
		int a = -4, b = -2;
		assertEquals(2, sc.divided(a, b), 0.01);
	}
	
	@Test
	public void onePositiveOneNegativeDivision() {
		int a = 4, b = -2;
		assertEquals(-2, sc.divided(a, b), 0.01);
	}
	
	public void dividedByZero() {
		int a = 4, b = 0;
		assertEquals(0, sc.divided(a, b), 0.01);
	}

	
}
