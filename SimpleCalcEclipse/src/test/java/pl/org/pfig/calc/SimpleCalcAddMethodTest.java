package pl.org.pfig.calc;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SimpleCalcAddMethodTest {
	
	private SimpleCalc sc;
	
	@Before
	public void doStuff(){
		sc = new SimpleCalc();
	}

	@Test
	public void whenTwoPositiveNumbersAreGivenPositiveNumberAreSum() {
		int a = 3, b = 6;
		assertEquals(9, sc.add(a, b));
	}
	@Test(expected=Exception.class)
	public void whenExThrowMethodIsUsedExceptionIsThrown() throws Exception {
		sc.exThrow();
	}
	
	@Test
	public void whenTwoNegativeNumbersAreGivenNegativeNumbersAreSum() {
		int a = -4, b = -8;
		assertEquals(-12, sc.add(a, b));
	}
	
	@Test
	public void oneNegativeOnePositiveSum() {
		int a = -4, b = 8;
		assertEquals(4, sc.add(a, b));
	}
	
	@Test
	public void onePositiveOneNegativeSum() {
	int a = 4, b = -2;
	assertEquals(2, sc.add(a, b));
	}
	
	@Test
	public void whenTwoMaxIntegersAreGivenPositiveNumberAreExpected() {
		assertTrue("Out of range", sc.add(Integer.MAX_VALUE, Integer.MAX_VALUE) > 0);
	}
	
	

}
