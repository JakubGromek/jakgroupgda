package pl.org.pfig.calc;

import static org.junit.Assert.*;

import org.junit.Test;

public class SimpleCalcSubstractMethodTest {

	@Test
	public void oneNegativeOnePositiveSubstract() {
		int a = -4, b = 8;
		SimpleCalc sc = new SimpleCalc();
		assertEquals(-12, sc.substraction(a, b));
	}
	
	@Test
	public void onePositiveOnePositiveSubstract() {
		int a = 4, b = 8;
		SimpleCalc sc = new SimpleCalc();
		assertEquals(-4, sc.substraction(a, b));
	}
	
	@Test
	public void oneNegativeOneNegativeSubstract() {
		int a = -4, b = -8;
		SimpleCalc sc = new SimpleCalc();
		assertEquals(4, sc.substraction(a, b));
	}
	
	@Test
	public void onePositiveOneNegativeSubstract() {
		int a = 4, b = -8;
		SimpleCalc sc = new SimpleCalc();
		assertEquals(12, sc.substraction(a, b));
	}

}
