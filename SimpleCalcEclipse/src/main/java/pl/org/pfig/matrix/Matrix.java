package pl.org.pfig.matrix;

public class Matrix {

	// 1. Napisz funkcj� drukuj�c� tablic� dwuwymiarow�, tak aby zosta�a
	// wydrukowana jako macierz.
	// Wej�cie:
	// int[][] m = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
	// Wyj�cie:
	// 1 2 3
	// 4 5 6
	// 7 8 9

	public int[][] print2DArray(int[][] m) {

		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				System.out.printf("%3d ", m[i][j]);
			}
			System.out.print("\n");
		}
		return m;
	}

	// 2. Napisz funkcj�, kt�ra przyjmie jako parametr tablic� dwuwymiarow� i
	// przypisze 1 jako jej warto�ci
	// w ka�dym z p�l - tak� konstrukcj� nazywamy macierz� jednostkow�. Funkcja
	// powinna zwraca�
	// uzupe�nion� tablic� dwuwymiarow�.

	public int[][] identityMatrix(int[][] m) {
		if (m.length == m[0].length) {
			for (int i = 0; i < m.length; i++) {
				for (int j = 0; j < m[i].length; j++) {
					if (i == j) {
						m[i][j] = 1;
					}
				}
			}
		}

		return m;
	}

	// 3. Napisz funkcj�, kt�ra przyjmie jako parametr tablic� dwuwymiarow� i
	// przypisze kolejne liczby od 1
	// jako jej warto�ci. Funkcja powinna zwraca� uzupe�nion� tablic�
	// dwuwymiarow�.

	public int[][] indexedMatrix(int[][] m) {
		int counter = 1;

		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				m[i][j] = counter++;
			}
		}

		return m;
	}

	// 4. Napisz funkcj� sprawdzaj�c� czy dwie przekazane jako argumenty tablice
	// dwuwymiarowe maj� takie wymiary (tj. zar�wno w jednej jak i w drugiej
	// tablicy ilo�� wierszy i kolumn jest taka sama).

	public boolean isEqualDimension(int[][] a, int[][] b) {

		if (a.length != b.length) {
			return false;
		}

		for (int i = 0; i < a.length; i++) {
			if (a[i].length != b[i].length) {
				return false;
			}
		}

		return true;
	}

	// 5. Napisz funkcj�, kt�ra doda do siebie dwie macierze. Funkcja powinna
	// zwraca� sum� macierzy (jako macierz). Zwr�� uwag� na to, �e obie macierze
	// musz� mie� takie same wymiary.

	public int[][] addMatrix(int[][] a, int[][] b) throws IllegalArgumentException {
		if (!this.isEqualDimension(a, b)) {
			throw new IllegalArgumentException("Tablice nie s� jednakowej d�ugo�ci!");
		}

		int[][] c = new int[a.length][a[0].length];

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				c[i][j] = a[i][j] + b[i][j];
			}
		}

		return c;
	}

	// 6. Napisz funkcj�, kt�ra odejmie od siebie dwie macierze. Funkcja powinna
	// zwraca� r�nic� macierzy
	// (jako macierz). Zwr�� uwag� na to, �e obie macierze musz� mie� takie same
	// wymiary.

	public int[][] substractMatrix(int[][] a, int[][] b) throws IllegalArgumentException {
		if (!this.isEqualDimension(a, b)) {
			throw new IllegalArgumentException("Tablice nie s� jednakowej d�ugo�ci!");
		}

		int[][] c = new int[a.length][a[0].length];
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				c[i][j] = a[i][j] - b[i][j];
			}
		}

		return c;
	}

	// 7. Napisz funkcj�, kt�ra przemno�y macierz przez liczb� (ka�da z kom�rek
	// musi zosta� pomno�ona przez t� liczb�).
	public int[][] multiplicateMatrix(int[][] m, int n) {

		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				m[i][j] *= n;
			}
		}
		return m;
	}

	// 8. Napisz funkcj�, kt�ra pozowli na realizacj� tranpozycji macierzy
	// (tranpozycja, w skr�cie zamiana wierszy z kolumnami /tak�e je�li chodzi o
	// wymiary/).

	public int[][] transpose(int[][] m) {
		int[][] result = new int[m[0].length][m.length];
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[i].length; j++) {
				result[i][j] = m[j][i];
			}
		}
		return result;
	}

	// 9. Napisz funkcj� sprawdzaj�c�, czy macierz jest symetryczna ( tj. A(T) =
	// A ).

	public boolean isSymetric(int[][] a) {
		if (a.length != a[0].length) {
			return false;
		}

		int[][] b = this.transpose(a);

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				if ((i != j) && (a[i][j] != b[j][i])) {
					return false;
				}
			}
		}

		return true;
	}

	// *10. Napisz funkcj� realizuj�c� mno�enie macierzy przez macierz.

	public int[][] multiplicateTwoMatrix(int[][] a, int[][] b) throws IllegalArgumentException {
		if (a.length != b[0].length) {
			throw new IllegalArgumentException("Tabele maj� nieodpowiednie rozmiary!");
		}

		int[][] c = new int[a.length][b[0].length];

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < c[0].length; j++) {
				for (int k = 0; k < a[0].length; k++) {
					c[i][j] += a[i][k] * b[k][j];
				}
			}
		}
		return c;

	}

	// *11. Napisz funkcj� obliczaj�c� wyznacznik macierzy 3x3 (ze sprawdzeniem
	// wymiar�w przekazanej macierzy).
	
	public int indicator(int[][] m) throws IllegalArgumentException {

		if (m.length != m[0].length) {
			throw new IllegalArgumentException("Nie mo�na wyznaczy� wyznacznika dla macierzy niekwadratowej!");
		}

		int result = 0;

		if (m.length == 1) {
			result = m[0][0];
		} else {

			for (int i = 0; i < m[0].length; i++) {
				int[][] temp = new int[m.length - 1][m[0].length - 1];
				
				for (int j = 1, jm = 0; j < m[0].length; j++) {
				
					for (int k = 0, km = 0; k < m[0].length; k++) {
						if (j != 0 && k != i) {

							temp[jm][km] = m[j][k];
							km++;
						}
					}
					
					jm++;
				
				}
	
				result += m[0][i] * Math.pow(-1, (1 + (i + 1))) * this.indicator(temp);
	
			}
		}
		return result;
	}

}