package pl.org.pfig.factoryThree;

import java.util.Random;

/**
 * Created by RENT on 2017-06-22.
 */
public class PersonFactory {

    public static Person getPerson(String name) {

        return new Person(name, "Gromek", new Random().nextInt(50)+18);
    }

}
