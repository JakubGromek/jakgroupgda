package pl.org.pfig;


public class SingletonExample {
    private static SingletonExample _instance;

    private SingletonExample() {
    }

    public static SingletonExample get_instance() {
        if (_instance == null) {
            System.out.println("Tworze instancje");
            _instance = new SingletonExample();
        }
        System.out.println("Zwracam instancje, bo juz jest utworzona.");
        return _instance;
    }
}
