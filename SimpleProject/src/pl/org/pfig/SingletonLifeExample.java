package pl.org.pfig;

/**
 * Created by RENT on 2017-06-22.
 */
public class SingletonLifeExample {

    private static SingletonLifeExample _instance;
    private String name;

    private SingletonLifeExample(){}

    public static SingletonLifeExample get_instance(){
        if (_instance == null) {
            System.out.println("Wybieram kapitana");
            _instance = new SingletonLifeExample();
            System.out.println("Kapitan wybrany");
            System.out.println("Rzucamy monetą");
        }
        System.out.println("Zaczynamy mecz");
        return _instance;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
