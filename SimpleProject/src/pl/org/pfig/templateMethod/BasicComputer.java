package pl.org.pfig.templateMethod;

/**
 * Created by RENT on 2017-06-22.
 */
public abstract class BasicComputer {

    public void devices() {
        motherboard();
        processor();
        hardDrive();
        externalDevices();
        System.out.println();

    }

    public void motherboard() {
        System.out.println("Motherboard");
    }

    public void processor() {
        System.out.println("Processor");
    }

    public void hardDrive() {
        System.out.println("Hard drive");
    }

    public abstract void externalDevices();
}
