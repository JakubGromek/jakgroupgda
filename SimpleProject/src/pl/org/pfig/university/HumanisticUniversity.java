package pl.org.pfig.university;

/**
 * Created by RENT on 2017-06-22.
 */
public abstract class HumanisticUniversity {

    public void subjects(){
        polishLang();
        philosophy();
        history();
        addictionalSubjects();
        System.out.println();

    }

    public void polishLang(){
        System.out.println("Polish Language");
    }
    public void history(){
        System.out.println("History");
    }
    public void philosophy(){
        System.out.println("Philosophy");
    }

    public abstract void addictionalSubjects();



}
