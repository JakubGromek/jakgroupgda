package pl.org.pfig.carsFactory;

/**
 * Created by RENT on 2017-06-22.
 */
public class CarsFactory {
    public static CarsInterface getCar(String car) {
        car = car.toLowerCase();
        switch (car) {
            case "car":
                return new Car();
            case "truck":
                return new Truck();

        }
        throw new IllegalArgumentException("Unknow car.");

    }
}
