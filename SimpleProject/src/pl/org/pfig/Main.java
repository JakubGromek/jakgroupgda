package pl.org.pfig;

import pl.org.pfig.carsFactory.CarsFactory;
import pl.org.pfig.carsFactory.CarsInterface;
import pl.org.pfig.factory.AnimalFactory;
import pl.org.pfig.factory.AnimalInterface;
import pl.org.pfig.factoryOtherWay.Coupon;
import pl.org.pfig.factoryOtherWay.CouponFactory;
import pl.org.pfig.factoryThree.Person;
import pl.org.pfig.factoryThree.PersonFactory;
import pl.org.pfig.fluidInterface.Beer;
import pl.org.pfig.fluidInterface.People;
import pl.org.pfig.templateMethod.Laptop;
import pl.org.pfig.templateMethod.MidiTower;
import pl.org.pfig.templateMethod.PersonalComputer;
import pl.org.pfig.university.EnglishPhilology;
import pl.org.pfig.university.History;
import pl.org.pfig.university.ScandinavianPhilology;

import java.lang.ref.SoftReference;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

//        SingletonExample se1 = SingletonExample.get_instance();
//        SingletonExample se2 = SingletonExample.get_instance();
//
//        SingletonLifeExample s1 = SingletonLifeExample.get_instance();
//        s1.setName("Kuba");
//        SingletonLifeExample s2 = SingletonLifeExample.get_instance();
//        System.out.println(s2.getName());

        /* Template method pattern*/
//        Laptop laptop = new Laptop();
//        System.out.println("Laptop:");
//        laptop.devices();
//
//        MidiTower midiTower = new MidiTower();
//        System.out.println("MidiTower Computer:");
//        midiTower.devices();
//
//        PersonalComputer personalComputer = new PersonalComputer();
//        System.out.println("PC:");
//        personalComputer.devices();
//
//        History history = new History();
//        System.out.println("History:");
//        history.subjects();
//
//        EnglishPhilology englishPhilology = new EnglishPhilology();
//        System.out.println("English Philology:");
//        englishPhilology.subjects();
//
//        ScandinavianPhilology scandinavianPhilology = new ScandinavianPhilology();
//        System.out.println("Scandinavian Philology:");
//        scandinavianPhilology.subjects();

//        AnimalInterface cat = AnimalFactory.getAnimal("cat");
//        cat.getSound();
//        AnimalInterface dog = AnimalFactory.getAnimal("dog");
//        dog.getSound();
//        AnimalInterface frog = AnimalFactory.getAnimal("frog");
//        frog.getSound();
//        AnimalInterface tiger = AnimalFactory.getAnimal("tiger");

//            int[] numbers = new int[6];
//            for(int i=0; i<numbers.length; i++){
//                numbers[i] = new Random().nextInt(48)+1;
//            }
//
//        Coupon c = CouponFactory.getCoupon(numbers);
//        System.out.println(c.getA());
//
//        CarsInterface car = CarsFactory.getCar("car");
//        car.getBrand();
//        CarsInterface truck = CarsFactory.getCar("truck");
//        truck.getBrand();

        Person kuba = PersonFactory.getPerson("Kuba");
        System.out.println(kuba);

        /*Chaining*/
        Beer beer = new Beer();
        beer.setName("Tyskie").
                setPrice(2.50).
                setType("Lager").
                setTaste("Bitter");

        System.out.println(beer);
        System.out.println();

        /*Fluid Interface*/

        List<Person> ppl = new LinkedList<>();
        ppl.add(new Person("Paweł", "Testowy", 30));
        ppl.add(new Person("Gaweł", "Przykładowy", 44));
        ppl.add(new Person("Miron", "Testowy", 23));
        ppl.add(PersonFactory.getPerson("Klaudia"));
        ppl.add(kuba);

        People pp = new People().addGroup("staff", ppl);
        for (Person pers : pp.from("staff").lastname("Gromek").get()) {
            System.out.println(pers);
        }






    }
}
