package pl.org.pfig.cities.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class QueryHelper {
    public static String buildUpdateQuery(String tableName, Map<String, String> props) {
        String q = "UPDATE `" + tableName + "` SET ";
        String subquery = "";
        int id = Integer.valueOf(props.get("id"));
        props.remove("id");

        for(Map.Entry<String, String> e : props.entrySet()) {
            subquery += " `" + e.getKey() + "` = '" + e.getValue() + "',";
        }

        q += subquery.substring(0, subquery.length() - 1);
        q += " WHERE `id` = " + id;
        return q;
    }


//     INSERT INTO `student` ( id, name, lastname ) VALUES (NULL, "Paweł", "Testowy");
//     INSERT INTO `student` ( mapKeys ) VALUES ( mapValues )
    // czy mapa gwarantuje kolejność elementów?

    private static String generateBracketsValues(List<String> args, String escapeCharacter) {
        String ret = "( ";
        for (String str : args){
            if (escapeCharacter.length() > 0) ret += "'";
            ret += str;
            if (escapeCharacter.length() > 0) ret += "'";
            ret += ", ";
        }
        if (args.size() > 0) ret = ret.substring(0, ret.length()-2) + " ";
        ret += " )";
        return ret;
    }

//    private static String generateBracketsValues(boolean escape, String... args) {
//        String ret = "( ";
//        for (String str : args){
//            if (escape) ret += "'";
//            ret += str;
//            if (escape) ret += "'";
//            ret += ", ";
//        }
//        if (args.length > 0) ret = ret.substring(0, ret.length()-2) + " ";
//        ret += " )";
//        return ret;
//    }

    // Napiszmy metode, ktora przyjmie Liste Stringów, a zwróci stringa w postaci ( arg1, arg2, arg3 )
    // Metoda powinna przyjmować drugi parametr escape:String, który powinien być dodawana na końcu
    // i początku każdego z elementów
    //
    // +formatValues(List<String> values, String escapeChar):String

    public String delete (String tablename, int id){
        return "DELETE FROM `" + tablename+ "` WHERE `id` = "+id;
    }
    public String insert(String tableName, Map<String,String> props) {
        String ret = "INSERT INTO `"+tableName+"` ";
        List<String> keys = new ArrayList<>();
        List<String> values = new ArrayList<>();
        for (Map.Entry<String, String> e : props.entrySet()){
            keys.add(e.getKey());
            values.add(e.getValue());
        }

        return  ret;
    }

}