package pl.org.pfig.cities.resolver.student;

import pl.org.pfig.cities.model.Student;
import pl.org.pfig.cities.resolver.AbstractResolver;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class StudentResolver extends AbstractResolver<Student> {

    private Connection connection;

    @Override
    public void connect(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Student> get() {
        List<Student> listOfStudents = new ArrayList<>();
        try {
            String query = "SELECT * FROM `student`";
            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery(query);


            while (rs.next()) {
                listOfStudents.add(new Student(rs.getString("name"), rs.getString("lastname")));
            }
            rs.close();
            s.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return listOfStudents;
    }



}