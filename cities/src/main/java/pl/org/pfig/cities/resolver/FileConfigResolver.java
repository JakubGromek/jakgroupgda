package pl.org.pfig.cities.resolver;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class FileConfigResolver {

    public static Map<String, String> resolve(String filename) {
        HashMap<String, String> ret = null;
        try {
            Scanner s = new Scanner(new File("src/main/resources/" + filename + ".config"));
            ret = new HashMap<>();
            while (s.hasNextLine()) {
                String line = s.nextLine();
                String[] splitted = line.split("=");
                ret.put(splitted[0].trim(), splitted[1].trim());
            }
            s.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }
}