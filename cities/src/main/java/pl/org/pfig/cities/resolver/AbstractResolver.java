package pl.org.pfig.cities.resolver;

import pl.org.pfig.cities.model.City;

import java.sql.Connection;
import java.util.List;

public abstract class AbstractResolver<T> {
    public abstract List<T> get();

    public abstract void connect(Connection connection);
}