package pl.org.pfig.cities.model;

/**
 * Created by RENT on 2017-07-06.
 */
public class Student {

String name;
String lastname;

    public Student(){}

    public Student(String name, String lastname) {
        this.name = name;
        this.lastname = lastname;
    }

    public String getName() {
        return name;
    }

    public Student setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public Student setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public String getStudent() {
        return name + " " + lastname;
    }
}
