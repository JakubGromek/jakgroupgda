package pl.org.pfig.cities;

import pl.org.pfig.cities.connector.DatabaseConnector;
import pl.org.pfig.cities.model.City;
import pl.org.pfig.cities.model.Student;
import pl.org.pfig.cities.resolver.FileConfigResolver;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import pl.org.pfig.cities.app.Application;
import pl.org.pfig.cities.resolver.CityResolver;
import java.util.HashMap;
import pl.org.pfig.cities.helper.QueryHelper;
import pl.org.pfig.cities.resolver.student.StudentResolver;

/**
 * Created by RENT on 2017-07-06.
 */
public class Main {


    public static void main(String[] args) throws SQLException {

        Application app = new Application();
        app.register("student", new StudentResolver());

        List<Student> listOfStudents2 = app.service("student").get();
        for (Student s : listOfStudents2){
            System.out.println(s.getName()+" "+s.getLastname());
        }
        System.exit(0);

        //       app.register("city", new CityResolver());

//                    List<City> listOfCities2 = app.service("city").get();
//
//                    for(City c : listOfCities2) {
//                        System.out.println(c.getId() + ". " + c.getCity());
//                    }
//
//                    System.exit(0);

//        List<Student> listOfStudents = new ArrayList<>();
//        try {
//            Connection connection = DatabaseConnector.getInstance().getConnection();
//            String query = "SELECT * FROM `student`; ";
//
//
//            Statement s = connection.createStatement();
//            ResultSet rs = s.executeQuery(query);
//            while (rs.next()) {
//                listOfStudents.add(new Student(rs.getString("name"), rs.getString("lastname")));
//            }
//
//            rs.close();
//            s.close();
//            //connection.close();
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }


//        for (Student c : listOfStudents) {
//            System.out.println(">" + c.getStudent());
//        }
//        try {
//            Connection connection = DatabaseConnector.getInstance().getConnection();
//            String query = "SELECT * FROM `student`; ";
//
//
//            Statement s = connection.createStatement();
//            ResultSet rs = s.executeQuery(query);
//            while (rs.next()) {
//                System.out.println(rs.getString("name") + " " + rs.getString("lastname"));
//            }
//
//
//            rs.close();
//            s.close();
//        }catch(SQLException e) {
//            System.out.println(e.getMessage());
//        }
//               try {
//            Connection c = DatabaseConnector.getInstance().getConnection();
//            Statement s = c.createStatement();
//            s.execute("CREATE TABLE `subject` (ID INT AUTO_INCREMENT PRIMARY KEY NOT NULL, name VARCHAR(100) NOT NULL);");
//            s.close();
//        } catch (SQLException e){
//            System.out.println(e.getMessage());
//        }

//        try {
//            Connection c = DatabaseConnector.getInstance().getConnection();
//            Statement s = c.createStatement();
//            s.execute("UPDATE `student` set lastname = NULL WHERE lastname = 'Gromek';");
//            s.close();
//        } catch (SQLException e){
//            System.out.println(e.getMessage());
//        }


//                    Map<String, String> prop = new HashMap<>();
//            prop.put("id", "2");
//            prop.put("name", "Brzęszczyszczykiewicz");
//            prop.put("lastname", "Tester");
//            prop.put("innaklolumna", "innawartosc");
//
//        System.out.println(prop.get("name"));
//
//        System.exit(0);
//
//
//            System.out.println(QueryHelper.buildUpdateQuery("student", prop));

//        Map<String, String> config = FileConfigResolver.resolve("db");
//        System.out.println(config.get("dbhost"));
//        for (Map.Entry e : config.entrySet()) {
//            System.out.println(e.getKey() + " > = < " + e.getValue());
//        }
//        System.exit(0);
//        try {
//            Connection c = DatabaseConnector.getInstance().getConnection();
//            String query = "update `city` set `city` = ? where `city` = ? ";
//            PreparedStatement ps = c.prepareStatement(query);
//            ps.setString(2,"Hel");
//            ps.setString(1,"Zakopane");
//            int countRows = ps.executeUpdate();
//            System.out.println("Nadpisalem "+ countRows + " wiersz(y).");
//            ps.close();
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }

            // dbhost, dbport, dbname, dbuser, dbpass
            // insert
//        try {
//            Connection c = DatabaseConnector.getInstance().getConnection();
//            Statement s = c.createStatement();
//            s.execute("INSERT INTO  `city` (city) VALUES ('Hel');");
//            s.close();
//        } catch (SQLException e){
//            System.out.println(e.getMessage());
//        }
            // update i delete
//        try {
//            Connection c = DatabaseConnector.getInstance().getConnection();
//            Statement s = c.createStatement();
//            s.execute("UPDATE `city` set city = NULL WHERE city = 'Gdynia';");
//            s.close();
//        } catch (SQLException e){
//            System.out.println(e.getMessage());
//        }
//        try {
//            Connection c = DatabaseConnector.getInstance().getConnection();
//            Statement s = c.createStatement();
//            s.execute("DELETE FROM `city` WHERE city IS NULL ;");
//            s.close();
//        } catch (SQLException e){
//            System.out.println(e.getMessage());
//        }
//        // select
//        List<City> listOfCities = new ArrayList<>();
//        try {
//            Connection connection = DatabaseConnector.getInstance().getConnection();
//            String query = "SELECT * FROM `city`; ";
//
//
//            Statement s = connection.createStatement();
//            ResultSet rs = s.executeQuery(query);
//            while (rs.next()){
//                listOfCities.add(new City(rs.getInt("id"),rs.getString("city")));
//            }
//
//            rs.close();
//            s.close();
//            connection.close();
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }
//        for (City c : listOfCities){
            //       System.out.println(">" + c.getCity());
//        }
//        try {
//            Connection connection = DatabaseConnector.getInstance().getConnection();
//            String query = "SELECT * FROM `city`; ";
//
//
//            Statement s = connection.createStatement();
//            ResultSet rs = s.executeQuery(query);
//           while (rs.next()){
//               System.out.println(rs.getInt("id")+". "+ rs.getString("city"));
//           }
//
//
//           rs.close();
//           s.close();
//           connection.close();
//
//
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }


        }
    }

