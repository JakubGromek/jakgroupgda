package pl.org.pfig.cities.model;

/**
 * Created by RENT on 2017-07-06.
 */
public class City {
    private int id;
    private String city;


    public City(){}

    public City(int id, String city){
        this.id = id;
        this.city =city;
    }

    public int getId() {
        return id;
    }

    public City setId(int id) {
        this.id = id;
        return this;
    }

    public String getCity() {
        return city;
    }

    public City setCity(String city) {
        this.city = city;
        return this;
    }
}
