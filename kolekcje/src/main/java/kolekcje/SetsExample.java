package kolekcje;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class SetsExample {

	public static void main(String[] args) {
		int[] tab = { 5, 1000, 2, 1, 3000, 4, 2000, 2, 3, 2, 4, 5 };

		// Set<Integer> set = new HashSet<>();
		Set<Integer> set = new TreeSet<>();
		// set.addAll(Arrays.asList(1,2,1,3,4,2,2,3,2,4,5));
		// set.add(tab[0]);

		for (int t : tab) {
			set.add(t);
			// System.out.println(set);
		}
		System.out.println(set);
		System.out.println("Rozmiar zbioru: " + set.size());

		for (int i : set) {
			System.out.print(" " + i);
		}
		System.out.println();
		set.remove(1);
		set.remove(2);
		System.out.println("Zbi�r: ");
		for (int i : set) {
			System.out.print(" " + i);
		}
		System.out.println();
		System.out.println("Rozmiar nowy: " + set.size());

		Iterable<Integer> iterable = set;
		for (int i : iterable) {
			System.out.print(" "+i);
		}
		System.out.println();

		Iterator<Integer> iterator = set.iterator();
		System.out.println("Pojedy�czy element: ");
		System.out.println(iterator.next());
		System.out.println(set.iterator().next());

		System.out.println("Caly zbi�r: ");
		Iterator<Integer> it = set.iterator();
		while (it.hasNext()) {
			System.out.print(" " + it.next());
		}
		System.out.println();

	}

}
