package kolekcje;

import java.util.Arrays;
import java.util.TreeSet;

public class TreeSetExample {

	public static void main(String[] args) {
		TreeSet<Integer> treeSet = new TreeSet<>(Arrays.asList(1,2,3,4,5,6));
		System.out.println("Ca�y zbi�r: ");
		for(int i : treeSet){
			System.out.println(i);
		}
		System.out.println("Ca�y headSet(4): ");
		for(Integer i : treeSet.headSet(4)) {
			System.out.print(i);
		}
		System.out.println();
		System.out.println("Ca�y tailSet(3): ");
		for(Integer i : treeSet.tailSet(3)) {
			System.out.print(i);
		}
		
	}

}
