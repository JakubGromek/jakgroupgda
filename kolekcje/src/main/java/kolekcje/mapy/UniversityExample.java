package kolekcje.mapy;

public class UniversityExample {

	public static void main(String[] args) {

		
		University university = new University();
		university.addStudent(4533, "Jakub", "Gromek");
		university.addStudent(1233, "Adam", "Nowak");
		university.addStudent(3321, "Anna", "Kowalska");
		
		System.out.println(university.studentsNumber());
		
		System.out.println(university.studentExist(4533));
		
		university.showAll();
		
		System.out.println(university.getStudent(4533));
	}

}
