package kolekcje.mapy;

import java.util.HashMap;
import java.util.Map;

public class University {
	
	private Map<Integer, Student> students = new HashMap<>(); 
	
	public void addStudent(int numerIndeksu, String imie, String nazwisko) {
		Student student = new Student(numerIndeksu, imie, nazwisko);
		students.put(student.getNumerIndeksu(), student);
		
	}
	
	public boolean studentExist(int numerIndeksu) {
		// delegacja
		return students.containsKey(numerIndeksu);
	}
	
	public Student getStudent(int numerIndeksu) {
		return students.get(numerIndeksu);
	}
	
	public int studentsNumber () {
		return students.size();
	}
	
	public void showAll (){
		for (Student s : students.values()) {
			System.out.println(s.getNumerIndeksu()+ " "+ s.getImie()+" "+s.getNazwisko());
		}
	}
	
	

}
