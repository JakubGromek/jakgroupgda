package kolekcje.mapy;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class StudentMap {
	
	
	public static void main(String[] args) {



		Map<Integer, Student> student = new TreeMap<>();
		student.put(1234, new Student(1234, "Jan", "Kowalski"));
		student.put(3333, new Student(3333,"Adam", "Nowak"));
		student.put(4442, new Student(4442,"Jakub","Gromek"));
		
		//System.out.println(student.getOrDefault(11121, "Nie ma takiego studenta."));
		for (Entry<Integer, Student> s : student.entrySet()) {
			System.out.println(s);
		}
		System.out.println();
		System.out.println(student.get(3333));
		System.out.println(student.getOrDefault(22311, new Student(22311,"","")));
		System.out.print("Czy istnieje student o indeksie 22311? -");
		if(student.containsKey(22311) == false){
			System.out.println(" Nie");
		}
		System.out.print("Czy istnieje student o indeksie 3333? -");
		if(student.containsKey(3333) != false){
			System.out.println(" Tak");
		}
			
		System.out.println("Liczba student�w: "+student.size());
	}

}
