package kolekcje.mapy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Student {
	private final int numerIndeksu;
	private final String imie;
	private final String nazwisko;

	public Student(int numerIndeksu, String imie, String nazwisko) {
		this.numerIndeksu = numerIndeksu;
		this.imie = imie;
		this.nazwisko = nazwisko;

	}

	@Override
	public String toString() {
		return " Student, NumerIndeksu=" + numerIndeksu + ", Imie: " + imie + ", Nazwisko: " + nazwisko + "";
	}

	public int getNumerIndeksu() {
		return numerIndeksu;
	}

	public String getImie() {
		return imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

}
