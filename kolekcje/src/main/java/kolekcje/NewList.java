package kolekcje;

import java.util.ArrayList;
import java.util.List;

public class NewList {
	public static void main(String[] args) {

		List<Integer> list = new ArrayList<Integer>();
		list.add(1);
		list.add(2);
		list.add(4);
		list.add(2);
		list.add(5);
		list.add(12);
		list.add(3);
		list.add(2);
		for (int l : list) {
			System.out.print(" " + l);
		}
		System.out.println();

		int[] tablica = { 4, 2, 2, 1, 5, 29, 3, 8 };
		for (int t : tablica) {
			System.out.print(" " + t);
		}
		System.out.println();
		System.out.println(countDuplicates(tablica, list));

	}

	public static int countDuplicates(int[] tablica, List<Integer> list) {
		int count = 0;

		for (int i = 0; i < list.size(); i++) {
				if (tablica[i] == list.get(i)) {
					count++;
				}
		}

		return count;
	}
}
