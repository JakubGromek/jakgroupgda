package kolekcje;

import java.util.HashSet;
import java.util.Set;

public class StringDuplicates {
	
//	public static boolean containDuplicates(  ){
//		return false;
//	}

	public static boolean containDuplicates(String znaki) {
		Set<Character> set = new HashSet<>();

		
		for(char s : znaki.toCharArray()) {
			set.add(s);
		}

		return set.size() != znaki.length();
	}
	
	

}
