package kolekcje;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ParaLiczbExample {

	public static void main(String[] args) {
		Set<ParaLiczb> para = new HashSet<>();    ///{(1,2), (2,1), (1,1), (1,2)}. 
		para.add(new ParaLiczb(1,2));
		para.add(new ParaLiczb(2,1));
		para.add(new ParaLiczb(1,1));
		para.add(new ParaLiczb(1,2));
		
		for(ParaLiczb pl : para) {
			System.out.println(pl);
		}
		
		
	}

}
