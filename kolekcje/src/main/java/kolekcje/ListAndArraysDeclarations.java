package kolekcje;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListAndArraysDeclarations {

	public static void main(String[] args) {
		
		tablice();
		
		
		//szybko, ale tablica jest nieedytowalna
		List<Integer> lista1 = Arrays.asList(1,2,3);
		//Mo�na uczynic j� edytowaln�
		List<Integer> lista2 = new ArrayList<>(Arrays.asList(1,2,3));
		//Tradycyjnie
		List<Integer> lista3 = new ArrayList<>();
		lista3.add(1);
		lista3.add(2);
		lista3.add(3);

	}

	private static void tablice() {
		int[] tab1 = { 1, 2, 3, 4 };

		int[] tab2 = null;
		tab2 = new int[] { 1, 2 };
		tab2 = new int[2];
		tab2[0] = 1;
		tab2[1] = 2;

		int[][] tab2dim1 = { { 1, 2 }, { 3 }, { 2, 3, 5 } };
		// tablica tablic zer
		int[][] tab2dim2 = new int[4][3];

		// tablica tablic, ktore nie istniej� (null)
		int[][] tab2dim3 = new int[4][];
	}

}
