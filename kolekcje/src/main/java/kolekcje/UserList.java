package kolekcje;

import java.util.ArrayList;
import java.util.List;

public class UserList {

	public static void main(String[] args) {

		List<User> users = new ArrayList<User>();
		users.add(new User("admin", "admin"));
		users.add(new User("Stefan", "12345"));
		users.add(new User("admin", "admin"));
		
		for (User u : users) {
			System.out.println(u);
		}
		
		
	}

}
