package kolekcje;

import java.util.List;

public class ListMath {

	public static int sum(List<Integer> numbers) {
		int sum = numbers.get(0) + numbers.get(1) + numbers.get(2);
		return sum;
	}

	public static int sum2(List<Integer> numbers2) {
		int sum = 0;
		for (int i : numbers2) {
			sum += i;
		}
		return sum;
	}

	public static int iloczyn(List<Integer> numbers) {
		int sum = numbers.get(0) * numbers.get(1) * numbers.get(2);
		return sum;
	}

	public static int srednia(List<Integer> numbers2) {
		int sum = 0;
		for (int i : numbers2) {
			sum += i;
		}
		return sum / numbers2.size();
	}

	public static int srednia2(List<Integer> numbers) {
		
		return sum(numbers)/numbers.size();
			

	}
}
