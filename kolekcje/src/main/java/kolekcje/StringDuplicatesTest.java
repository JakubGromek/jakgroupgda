package kolekcje;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class StringDuplicatesTest {

	
	@Test
	public void containDuplicatesTest() {
		
		
		assert StringDuplicates.containDuplicates("abcd") == false;
		assert StringDuplicates.containDuplicates("abcc") == true;
		
	}
}
