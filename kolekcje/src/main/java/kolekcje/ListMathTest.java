package kolekcje;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ListMathTest {

	@Test
	public void sumTest() {
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(4);
		numbers.add(5);
		numbers.add(6);
		List<Integer> numbers2 = new ArrayList<Integer>();
		numbers2.add(4);
		numbers2.add(4);
		numbers2.add(4);
		numbers2.add(4);

		assert ListMath.sum(numbers) == 15;
		assert ListMath.sum2(numbers2) == 16;
		assert ListMath.iloczyn(numbers) == 120;
		assert ListMath.srednia(numbers2) == 4;
		assert ListMath.srednia2(numbers) == 5;

	}

}
