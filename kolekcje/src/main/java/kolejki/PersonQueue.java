package kolejki;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class PersonQueue   {

	public static void main(String[] args) {
		
		Queue<Person> persons = new PriorityQueue<>(new Comparator<Person>() {

			@Override
			public int compare(Person o1, Person o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		persons.offer(new Person("Adam", 44));
		persons.offer(new Person("Kuba", 29));
		persons.offer(new Person("Ania", 22));	
		persons.offer(new Person("Jan", 31));
		
		while(!persons.isEmpty()){
			System.out.println(persons.poll());
			
		}
			

		

	}

}
