package kolejki;

import java.security.Permissions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

public class SortowanieListy {

	public static void main(String[] args) {

		List<Person> persons = new ArrayList<>();
		
		persons.add(new Person("Adam", 44));
		persons.add(new Person("Kuba", 29));
		persons.add(new Person("Ania", 22));	
		persons.add(new Person("Jan", 31));
		
		System.out.println("Przed sortowaniem: ");
		for (Person person : persons) {
			System.out.println(person);
		}
		
//		Collections.sort(persons);
//		System.out.println("Po sortowaniu: ");
//		for (Person person : persons) {
//			System.out.println(person);
//		}
		Collections.shuffle(persons);
		System.out.println("Po tasowaniu : ");
		for (Person person : persons) {
			System.out.println(person);
		}
		Collections.reverse(persons);
		System.out.println("Po odwroceniu : ");
		for (Person person : persons) {
			System.out.println(person);
		}
		
	}

}
