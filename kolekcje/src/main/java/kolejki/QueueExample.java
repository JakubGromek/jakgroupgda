package kolejki;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class QueueExample {

	public static void main(String[] args) {

//		Queue<Integer> queue = new LinkedList<>();
//		queue.add(1);
//		queue.add(21);
//		queue.add(2);
//		queue.add(10);
//		queue.add(12);
//
//		while (!queue.isEmpty()) {
//			System.out.println(queue.poll());
//		}
//		System.out.println(queue.poll());
		
		
		Queue<Integer> queue2 = new PriorityQueue<>();
		queue2.add(1);
		queue2.add(21);
		queue2.add(2);
		queue2.add(10);
		queue2.add(12);

		while (!queue2.isEmpty()) {
			System.out.println(queue2.poll());
		}
		System.out.println(queue2.poll());
		

	}

}
