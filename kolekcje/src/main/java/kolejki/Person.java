package kolejki;

public class Person  {

	private final String name;
	private final int age;

	@Override
	public String toString() {
		return "Imie=" + name + ", Wiek=" + age + "";
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public Person(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	

	//@Override
	//public int compareTo(Person o) {
//		if (this.age > o.age) {
//			return 1;
//		} else if (this.age < o.age) {
//			return -1;
//		} else {
//			return 0;
//		}
		//return Integer.compare(this.age, o.age);
		//return this.age - o.age;
		
	//}
}
