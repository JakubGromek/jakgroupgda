package main.java.screenRecorder;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by RENT on 2017-06-29.
 */
public class Main {

    public static void main(String[] args) {

        ScreenRecorder screenRecorder = new ScreenRecorder();

        Profile p1 = new Profile(Codec.H263, Extension.AVI, Resolution.R1920X1080);
        Profile p2 = new Profile(Codec.H264, Extension.MKV, Resolution.R1680X1050);
        Profile p3 = new Profile(Codec.THEORA, Extension.MP4, Resolution.R1024X780);
        Profile p4 = createProfile();
        screenRecorder.addProfile(p1);
        screenRecorder.addProfile(p2);
        screenRecorder.addProfile(p3);
        screenRecorder.addProfile(p4);

        System.out.println("Lista profili: ");
        screenRecorder.listProfiles();
        Scanner sc = new Scanner(System.in);
        System.out.println("Wybierz profil: ");

        int np = sc.nextInt();
        screenRecorder.setProfile(np);

        System.out.println("Wybrany profil: "+ np);

    }

    private static Profile createProfile() {

        Scanner sc = new Scanner(System.in);

    System.out.println("Ustaw profil: ");
    System.out.println("Wybierz codec (H264, H263, THEORA, VR8): ");
    String codecType = sc.nextLine().toUpperCase();
    Codec codec = Codec.valueOf(codecType);

    System.out.println("Wybierz rozszerzenie (MKV, MP4, MP3, AVI): ");
    String exType = sc.nextLine().toUpperCase();
    Extension ex = Extension.valueOf(exType);

    System.out.println("Wybierz rozdzielczość (R1920x1080, R1680x1050, R1024x780): ");
    String resType = sc.nextLine().toUpperCase();
    Resolution resolution = Resolution.valueOf(resType);

    return new Profile(codec, ex, resolution);





    }
}
