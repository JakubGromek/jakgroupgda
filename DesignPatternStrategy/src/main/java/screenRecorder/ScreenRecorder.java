package main.java.screenRecorder;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-06-29.
 */
public class ScreenRecorder {

    //    ScreenRecorder - klasa którą zarządzamy.
//            Profile - klasa którą ustawiamy/tworzymy.
//    ScreenRecorder powinien posiadać listę profili, oraz aktualnie ustawiony profil (pole w klasie).
//    Profile - klasa którą tworzymy i dodajemy.
//    ScreenRecorder powinien mieć metodę
//- addProfile(Profile nowyProfil) - dodaje profil do listy
//- setProfile(int numerProfilu) - ustawia profil (pole) na wybrany z listy
//- listProfiles() - wylistowuje profile
//- startRecording() - rozpoczyna nagrywanie (tylko gdy jeszcze nie nagrywamy)
//- stopRecording() - zatrzymuje nagrywanie (tylko gdy już nagrywamy)
    private List<Profile> profiles = new LinkedList<>();

    private boolean isRecording = false;
    private Profile currentProfile;

    public void addProfile(Profile nowyProfil) {
        profiles.add(nowyProfil);
    }

    public int setProfile(int numerProfilu) {
        if (numerProfilu < 1 || numerProfilu > profiles.size()){
            System.out.println("Podany profil nie istnieje");
        }

        return numerProfilu;
    }

    public void listProfiles() {
        for (int i=0 ; i<profiles.size(); i++){
            System.out.println((i+1)+" : "+profiles.get(i));
        }
    }

//    public void startRecording() {
//        if (currentProfile == null){
//
//        }
//
//    }
//
//    public void stopRecording() {
//        if (isRecording) {
//            System.out.println("Recording stopped");
//        } else {
//            System.out.println("Nothing to stop");
//        }
//
//    }


}
