package userGenerator;

/**
 * Created by RENT on 2017-06-14.
 */
public enum UserSex {
    SEX_MALE(1, "Mężczyzna"),
    SEX_FEMALE(2, "Kobieta"),
    SEX_UNDEFINED(3, "Nieokreślono");

    private String name;
    private int id;

    UserSex(int id, String name){
        this.id = id;
        this.name = name;
    }

    public int getId(){
        return id;
    }

    public String getName() {
        return name;
    }
}
