package userGenerator;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class UserGenerator {
    private final String path = "C:\\Users\\RENT\\workspace\\DodatkoweZadania2/";


    public User getRandomUser() {
        //currentUser = new User();
        UserSex us = UserSex.SEX_MALE;
        if(new Random().nextInt(2) == 0){
            us = UserSex.SEX_FEMALE;
        }
        return getRandomUser(us);
    }

    public User getRandomUser(UserSex sex) {
        User currentUser = new User();

        currentUser.setSex(sex);
        String name = "";
        String lastname = "";

        if (sex.equals(UserSex.SEX_MALE)){
            name = getRandomLineFromFile("name_m.txt");
        } else {
            name = getRandomLineFromFile("name_f.txt");
        }
        currentUser.setName(name);

        lastname = getRandomLineFromFile("lastname.txt");
        currentUser.setSecondName(lastname);

        currentUser.setPhone(getRandomPhone());
        currentUser.setCCN(getRandomCCN());

        currentUser.setAddress(getRandomAddress());
        String dt = (getRandomBirthDate());
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        try {
            currentUser.setBirthDate(sdf.parse(dt));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        currentUser.setPESEL(getRandomPESEL(currentUser));

        return currentUser;
    }

    public String getRandomPhone() {
        int phone = new Random().nextInt(999999999);
        if (phone > 100000000) {
            return "" + phone;
        }
        return ""+phone+ new Random().nextInt(9)+"";

    }

    public String getRandomCCN() {

        int CCN = new Random().nextInt(999999);
        if (CCN > 100000 ){
            return "" + CCN+CCN ;
        }
        return "" +CCN+ new Random().nextInt(9)+""+CCN+ new Random().nextInt(9)+"";


    }

    public String getRandomPESEL(User user){
        Date birth = user.getBirthDate();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
          String dt = sdf.format(birth).replace(".", "");
          String[] dropped = dt.split("");
          String day= dropped[0]+dropped[1], month= dropped[2]+dropped[3], year= dropped[6]+dropped[7];
          int next3 = new Random().nextInt(999);

          //User sex = getRandomUser();
        int s = new Random().nextInt(10);
          if (user.getSex() == UserSex.SEX_MALE){
             if(s % 2 == 0) s++;
          } else {
             if(s % 2 != 0) s++;
          }


        s = s % 10;

        if (next3 > 100 ){
            return year+" "+month+" "+day+" "+next3+" "+ s ;
        }
            return year+" "+month+" "+day+" "+next3+ new Random().nextInt(9)+" "+s;
    }

    public String getRandomBirthDate() {
        int[] daysInMonth = {31,28,31,30,31,30,31,31,30,31,30,31};
        int year = 1890 + new Random().nextInt(120);
        int month = new Random().nextInt(12)+1;
        int day = new Random().nextInt(daysInMonth[month-1]+1);
        if (year% 4 ==0 ){
            daysInMonth[1]++;
        }

        return leadZero(day)+"."+leadZero(month)+"."+ year;
    }

    private String leadZero(int arg) {
        if (arg <10){
            return "0"+arg;
        } else {
            return "" + arg;
        }
    }

    public Address getRandomAddress() {
        Address adr = new Address();
        String[] cityData = getRandomLineFromFile("city.txt").split(" \t ");
        adr.setCountry("Poland");
        adr.setCity(cityData[0]);
        adr.setZipcode(cityData[1]);
        adr.setStreet("ul."+getRandomLineFromFile("street.txt"));
        adr.setNumber(""+ (new Random().nextInt(100)+1));
        return  adr;
    }


    public int countLines(String filename) {
        int lines = 0;
        try (Scanner sc = new Scanner(new File(path+filename))) {
            while(sc.hasNextLine()) {
                lines++;
                sc.nextLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
        return lines;

    }

    public String getRandomLineFromFile(String filename) {

        int allLines = countLines(filename);
        int line = new Random().nextInt(allLines) +1;
        int currLine = 1;
        String currentLineContent = "";
        try (Scanner sc = new Scanner(new File(path + filename))) {
            while (sc.hasNextLine()) {
                currLine++;
                currentLineContent = sc.nextLine();
                if (line == currLine) {
                    return currentLineContent;
                }
            }

        } catch (FileNotFoundException e){
            System.out.println(e.getMessage());
        }


        return null;

    }

}
