package userGenerator;

import java.util.Date;

/**
 * Created by RENT on 2017-06-14.
 */
public class User {
    private String name;
    private String secondName;
    private Address address;
    private String phone;
    private UserSex sex;
    private String CCN;
    private String PESEL;
    private Date birthDate;

    public User(){}

    public User(String name, String secondName, Address address, String phone, UserSex sex, String CCN, String PESEL, Date birthDate) {
        this.name = name;
        this.secondName = secondName;
        this.address = address;
        this.phone = phone;
        this.sex = sex;
        this.CCN = CCN;
        this.PESEL = PESEL;
        this.birthDate = birthDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public UserSex getSex() {
        return sex;
    }

    public void setSex(UserSex sex) {
        this.sex = sex;
    }

    public String getCCN() {
        return CCN;
    }

    public void setCCN(String CCN) {
        this.CCN = CCN;
    }

    public String getPESEL() {
        return PESEL;
    }

    public void setPESEL(String PESEL) {
        this.PESEL = PESEL;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", secondName='" + secondName + '\'' +
                ", address=" + address +
                ", phone='" + phone + '\'' +
                ", sex=" + sex +
                ", CCN='" + CCN + '\'' +
                ", PESEL='" + PESEL + '\'' +
                ", birthDate=" + birthDate +
                '}';
    }
}
