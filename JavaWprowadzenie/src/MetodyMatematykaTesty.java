import org.junit.Test;

public class MetodyMatematykaTesty {
	
	@Test
	public void testIloczynu() {
		
		assert MetodyMatematyka.iloczyn(1, 1) == 1;
		assert MetodyMatematyka.iloczyn(0, 10) == 0;
		assert MetodyMatematyka.iloczyn(1, 10) == 10;
		assert MetodyMatematyka.iloczyn(3, 4) == 12;
		
	}

	
	@Test
	public void testMniejszej() {
		
		assert MetodyMatematyka.mniejsza(2, 3) == 2;
		assert MetodyMatematyka.mniejsza(4, 5) == 4;
		
		
	}
	
	
	@Test
	public void testMniejszej3() {
		
		assert MetodyMatematyka.mniejsza3(2, 3, 4) == 2;
		assert MetodyMatematyka.mniejsza3(4, 5, 8) == 4;
	}
}
