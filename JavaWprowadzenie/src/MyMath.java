
public class MyMath {

	public static void main(String[] args) {
		
		System.out.println("Wieksza liczba : " + max(2, 5));
		System.out.println("Wieksza liczba : " + max(2.0, 5.0));
		System.out.println("Mniejsza liczba : " + min(2, 5));
		System.out.println("Mniejsza liczba : " + min(2.0, 5.0));
		System.out.println("potega : " + test(2, 5));
		

	}
	
	
	public static int max(int a, int b) {
		if (a>b) {
			return a;
		} else {
			return b;
		}		
	}
	
	public static long max(long a, long b) {
		if (a>b) {
			return a;
		} else {
			return b;
		}		
	}
	
	public static double max(double a, double b) {
		if (a>b) {
			return a;
		} else {
			return b;
		}		
	}
	
	
	public static int min(int a, int b) {
		if (a<b) {
			return a;
		} else {
			return b;
		}	
	}
	
	public static double min(double a, double b) {
		if (a<b) {
			return a;
		} else {
			return b;
		}		
	}
	
	public static double abs(int a) {
		if (a>0) {
			return a;
		
		}	else  {	
			return -a;
		}
	}
	
	public static int pow(int a, int b) {
		
		int pow = 1;
				for (int i=1; i<=b; i++) {
					pow *=a;
				}
		return pow;
	}
	
public static double pow(double a, int b) {
		
		double pow = 1;
				for (int i=1; i<=b; i++) {
					pow *=a;
				}
		return pow;
	}

public static int test(int a, int b) {
	
	int test = 1;
			for (int i=1; i<=b; i++) {
				test = a*test; // albo test *= a
			}
	return test;
}

}
