
public class GeoMath {

	public static void main(String[] args) {
		
		
		System.out.println("Pole kwadratu : " + squareArea(4));
		System.out.println("Pole sze�cianu : " + cubeArea(7));
		System.out.println("Pole ko�a : " + circleArea(2));
		System.out.println("Obj�to�� walca : " + cylinderVolume(2, 2));
		System.out.println("Obj�to�� sto�ka : " + coneVolume(2, 2));
		System.out.println("Obj�to�� sze�cianu : " + cubeVolume(4));
		System.out.println("Obj�to�� ostros�upa o podstawie kwadratu : " + pyramidVolume(3, 2));

	}
	
	
	public static double squareArea(double a) {
		double squareArea = a * a;
		return squareArea;

	}
	
	
	public static double cubeArea(int a) {
		int cubeArea = 6 * a * a;
		return cubeArea;

	}
	
	public static double circleArea(double r) {
		
		return Math.PI * r * r;

	}
	
	
public static double cylinderVolume(double r, double h) {
		
		return Math.PI * (r * r) * h;

	}

public static double coneVolume(double r,  double h) {
	
	return  (Math.PI / 3) * (r * r) * h;
	

}

public static double cubeVolume(int a) {
	
	return a * a * a;

}

public static double pyramidVolume(double a, double h) {
	
	return (squareArea(a) / 3) * h;

}
}
