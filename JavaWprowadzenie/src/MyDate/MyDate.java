package MyDate;

public class MyDate {
	
	private int year = 0;
	private int month = 0;
	private int day = 0;
	
	String[] months = {"jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep","oct", "nov", "dec"};
	public String[] getMonths() {
		return months;
	}

	public void setMonths(String[] months) {
		this.months = months;
	}

	public MyDate(String[] months) {
		this.months = months;
	}

	//String[] days = {"mon", "tue", "wed", "thu", "fri", "sat", "sun"};
	//int[] dim = {31,28,31,30,31,30,31,30,31,30,31,30};
	
	//public MyDate() { }

	public MyDate(int year, int month, int day) {
		super();
		this.setYear(year);
		this.setMonth(month);
		this.setDay(day);
	}

	public MyDate() {
		// TODO Auto-generated constructor stub
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if(month >= 1 && month <= 12) {
			this.month = month;
		}
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}
	
	@Override
	public String toString() {
		return leadZero(this.year) + "." 
			 + leadZero(this.month) + "." 
	  		 + leadZero(this.day);
	}
	
	private String leadZero(int num) {
		if(num < 10) {
			return "0" + num;
		} else {
			return "" + num; // dlaczego mamy "" ? - ze wzgl�du na to, aby zwr�ci� liczb� jako string, a nie int
		}
	}
	

}