package MyTime;



	public class  MyTime{
		private int hour = 0;
		private int minute = 0;
		private int second = 0;
		
		public MyTime() { }
		
		public MyTime(int hour, int minute, int second) {
			setTime(hour, minute, second);
		}
		
		public void setTime(int hour, int minute, int second) {
			this.hour = hour;
			this.minute = minute;
			this.second = second;
		}

		@Override
		public String toString() {
			return leadZero(this.hour) + ":" 
				 + leadZero(this.minute) + ":" 
		  		 + leadZero(this.second);
		}
		
		private String leadZero(int num) {
			if(num < 10) {
				return "0" + num;
			} else {
				return "" + num; // dlaczego mamy "" ? - ze wzgl�du na to, aby zwr�ci� liczb� jako string, a nie int
			}
		}

		public int getHour() {
			return hour;
						
		}

		public void setHour(int hour) {
			if(hour >= 0 && hour <= 23) {
				this.hour = hour;
			} 
		}

		public int getMinute() {
			return minute;
		}

		public void setMinute(int minute) {
			if(minute >= 0 && minute <= 59) {
				this.minute = minute;
			}
		}

		public int getSecond() {
			return second;
		}

		public void setSecond(int second) {
			if(second >= 0 && second <= 59) {
				this.second = second;
			}
		}
		
		public MyTime nextHour() {
			int newHour = hour + 1;
			if(newHour == 24) {
				newHour = 0;
			}
			int newMinute = minute + 1;
			if(newMinute == 60) {
				newMinute = 0;
			}
			int newSecond = second +1;
			if(newSecond == 60) {
				newSecond = 0;
			}
			if (newMinute == 60) {
				newMinute = 0;
			}
			return new MyTime(newHour, newMinute, newSecond);
		}
		
		public MyTime prevHour() {
			int prevHour = hour - 1;
			if(prevHour == 24) {
				prevHour = 0;
			
			}
			int newMinute = minute - 1;
			if(newMinute == 60) {
				newMinute = 0;
			}
			int newSecond = second -1;
			if(newSecond == 60) {
				newSecond = 0;
			}
			
			if (newMinute == 00) {
				newMinute = 1;
			}
			return new MyTime(prevHour, newMinute, newSecond);
		}
		
		public MyTime nextSecond() {
			int newHour = hour,
				newMinute = minute,
				newSecond = second + 1;
			
			if(newSecond > 59) {
				newSecond = 0;
				newMinute ++;
			}
			
			if(newMinute >59) {
				newMinute = 0;
				newHour ++;
			}
			
			if(newHour >23) {
				newHour = 0;
			}
			return new MyTime(newHour, newMinute, newSecond);
		}
		
	}
