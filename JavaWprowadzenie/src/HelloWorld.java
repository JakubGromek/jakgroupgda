
public class HelloWorld {

	public static void main(String[] args) {
		byte b = 10;
		System.out.println("byte :" + b);
	
		int i = 42112; 
		System.out.println("int :" + i);		
		
		double d = 6.5;
		System.out.println("double :" + d);
		
		boolean bool = true; 
		System.out.println("bool :" +bool);
		
		float f = 11.4f;
		System.out.println("float :" +f);
	}

}
