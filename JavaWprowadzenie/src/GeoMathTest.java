import org.junit.Test;

public class GeoMathTest {
	
	
	
	@Test
	public void testPola() {
		
		assert GeoMath.squareArea(3) == 9;
		assert GeoMath.squareArea(4) == 16;
	}
	
	
	
	@Test
	public void testPolaSze�cianu() {
		
		assert GeoMath.cubeArea(7) == 294;
		assert GeoMath.cubeArea(4) != 20;
	}
	
	
	@Test
	public void testPolaKo�a() {
		
		assert GeoMath.circleArea(2) == 4*Math.PI;
		//assert GeoMath.circleArea(4) == 20;
	}
	
	@Test
	public void testObjetosciWalca() {
		
		assert GeoMath.cylinderVolume(2, 2) == 8*Math.PI;
		//assert GeoMath.circleArea(4) == 20;
	}
	
	@Test
	public void testObjeto�ciStozka() {
		
		assert GeoMath.coneVolume(2, 2) == (Math.PI / 3) * 8;
		assert GeoMath.coneVolume(2, 2) == 8.377580409572781;	}
	
	@Test
	public void testObjetociSzescianu() {
		
		assert GeoMath.cubeVolume(4) == 64;
		assert GeoMath.cubeVolume(4) != 20;
	}
	
	@Test
	public void testObjteociSOstroslupa() {
		
		assert GeoMath.pyramidVolume(4, 4) == 64;
		assert GeoMath.pyramidVolume(4, 4) != 20;
	}

}
