package pl.org.pfig.data;

public class llama implements AnimalInterface, Soundable {
	
	private String name;
	
	public llama(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public String getSound() {
		return "tra la la";
	}
	

}
