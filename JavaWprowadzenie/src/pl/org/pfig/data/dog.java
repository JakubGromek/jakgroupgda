package pl.org.pfig.data;

public class dog implements AnimalInterface {


		
		private String name;
		private int legs;

		public dog(String name) {
			super();
			this.name = name;
		}
		
		
		
		
		public int getLegs() {
			return legs;
		}




		public void setLegs(int legs) {
			if(legs > 0 && legs <5)
			this.legs = legs;
		}




		public String getName() {
			return this.name;
		}
		
		public dog newDog(String name) {
			return new dog(name);
		}

		@Override
		public String toString() {
			return "Dog: " +this.name;
		}
		
		

	

}
