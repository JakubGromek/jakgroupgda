package pl.org.pfig.main;

import pl.org.pfig.data.*;
//import pl.org.pfig.data.dog;

public class Main {
	
	public static void main(String[] args) {
		
		dog d1 = new dog("Burek");
		cat c1 = new cat("Mruczek");
		llama l1 = new llama("SuperLama");
		
		System.out.println("Piesek to: " + d1.getName());
		System.out.println("Kot to: " + c1.getName());
		System.out.println("Lama to: " + l1.getName());
		
		AnimalInterface[] animals = new AnimalInterface[3];
		animals[0] = d1;
		animals[1] = c1;
		animals[2] = l1;
		for (AnimalInterface ai : animals) {
			System.out.println("Zwierze nazywa sie: " + ai.getName() + " ("+ai.getLegs()+")");
			if (ai instanceof Soundable) {
				Soundable sound = (Soundable)ai;
				System.out.println("\tRobi: " +sound.getSound());
			}
		}
		
		System.out.println(d1);
	}

}
