import org.junit.Test;

public class MyMathTest {
	
	@Test
	public void testMaxInt() {
		
		assert MyMath.max(2, 4) == 4;

	}
	
	@Test
	public void testMaxLong() {
		
		assert MyMath.max(2L, 4L) == 4L;

	}
	
	@Test
	public void testMaxDouble() {
		
		assert MyMath.max(4.8, 4.1) == 4.8;

	}
	
	@Test
	public void testMinInt() {
		
		assert MyMath.min(2, 4) == 2;

	}
	
	@Test
	public void testMinDouble() {
		
		assert MyMath.min(2, 4) != 4;
		assert MyMath.min(2, 4) == 2;
		assert MyMath.min(5, 4) == 4;

	}
	
	@Test
	public void testAbsInt() {
		
		assert MyMath.abs(4) == 4;
		assert MyMath.abs(-4) == 4;
		assert MyMath.abs(0) == 0;

	}
	
	@Test
	public void testPowInt() {
		
		assert MyMath.pow(2, 2) == 4;

	}
	
	@Test
	public void testPowDouble() {
		
		assert MyMath.pow(3.0, 2) == 9.0;
		assert MyMath.pow(3, 2) == 9.0;
		assert MyMath.pow(3.0, 2) == 9;
		assert MyMath.pow(4.0, 2) == 16.0;

	}
}