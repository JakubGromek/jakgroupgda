package crud;

import entity.FootballLeague;

import java.util.List;

/**
 * Created by RENT on 2017-08-03.
 */
public interface CrudInterface<T> {
    void insertOrUpdate(T obj);
    void delete(T obj);
    T select(int id);
    List<T> select();
}