package menu;

import entity.FootballLeague;
import org.hibernate.Session;
import util.HibernateUtil;

import java.util.List;
import java.util.Scanner;

/**
 * Created by RENT on 2017-07-28.
 */
public class Menu {

    public static void menu(Option1 o1, Option2 o2, Option3 o3, Scanner cs) {
        do {
            System.out.println("----------------------------------------------------");
            System.out.println("MENU");
            System.out.println("1. Obstawianie cały sezon za tą samą stawkę.");
            System.out.println("2. Obstawianie progresywne remisów.");
            System.out.println("3. Losowanie trafionych kolejek.");
            System.out.println("4. Baza remisów z Ligi Angielskiej z lat 14-17.");
            System.out.println("5. Baza remisów z Drugiej ligi angielskiej (4-ta klasa rozgrywkowa) z lat 14-17.");
            System.out.println("6. Wyjście.");

            System.out.print("Wybierz: ");
            int menu = cs.nextInt();
            System.out.println("---------------------");

            switch (menu) {
                case 1:
                    opt1(o1, cs);
                    break;

                case 2:
                    System.out.println("Symulacja: 1");
                    opt2(o2, cs);
                    System.out.println("1. Wykonaj kolejną symulację.");
                    System.out.println("2. Wróć.");
                    System.out.println("--------------------------------------");
                    int menu2 = cs.nextInt();
                    switch (menu2) {
                        case 1:
                            System.out.println("Ile razy?");
                            int ile = cs.nextInt();
                            System.out.println("--------------------------------------");
                            for (int i = 1; i <= ile; i++) {
                                System.out.println("Symulacja: " + (i + 1));
                                opt2(o2, cs);

                            }
                            break;
                        case 2:
                            break;
                        default:
                            System.out.println("Nie ma takiej opcji!");
                            System.out.println("---------------------");
                    }
                    break;
                case 3:
                    opt3(o3, cs);
                    break;
                case 4:
                    System.out.println("1.Powyżej 9-ciu remisów");
                    System.out.println("2.Powyżej 11-tu remisów");
                    System.out.println("3.Powyżej 13-tu remisów");
                    System.out.println("4.Pełna tabela");
                    int draws = cs.nextInt();
                    switch (draws) {
                        case 1:
                            Option4.showDatabaseDraws10();
                            break;
                        case 2:
                            Option4.showDatabaseDraws12();
                            break;
                        case 3:
                            Option4.showDatabaseDraws14();
                            break;
                        case 4:
                            Option4.showFullTable();
                            break;
                        default:
                            System.out.println("Nie ma takiej opcji.");
                    }
                    break;
                case 5:
                    System.out.println("1.Powyżej 9-ciu remisów");
                    System.out.println("2.Powyżej 11-tu remisów");
                    System.out.println("3.Powyżej 13-tu remisów");
                    System.out.println("4.Pełna tabela");
                    int draws2 = cs.nextInt();
                    switch (draws2) {
                        case 1:
                            Option5.showDatabaseDraws10();
                            break;
                        case 2:
                            Option5.showDatabaseDraws12();
                            break;
                        case 3:
                            Option5.showDatabaseDraws14();
                            break;
                        case 4:
                            Option5.showFullTable();
                            break;
                        default:
                            System.out.println("Nie ma takiej opcji.");
                    }
                    break;

                case 6:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Nie ma takiej opcji!");
                    System.out.println("---------------------");

            }
        } while (true);
    }


    private static void opt3(Option3 o3, Scanner cs) {
        System.out.println("Wpisz ilość kolejek danej ligi: ");
        int kolejki = cs.nextInt();
        System.out.println("Losowość trafień remisów może wyglądać następująco: ");
        System.out.println(o3.roundChance(kolejki));
        System.out.println("---------------------");


    }

    private static void opt2(Option2 o2, Scanner cs) {
        System.out.println("Podaj stawkę: ");
        int stawka = cs.nextInt();
        System.out.println("Podaj o ile zwiększysz stawkę po każdej kolejce: ");
        int nstawka = cs.nextInt();
        System.out.println("Podaj średni kurs na mecz: ");
        double kurs = cs.nextDouble();
        System.out.println("Podaj ilość zdarzeń(meczy): ");
        int zdarzen = cs.nextInt();
        System.out.println("Po ilu kolejkach zakładasz, że wygrasz: ");
        double kolejki = cs.nextDouble();
        System.out.println("Sredni kurs to: " + o2.sKurs(kurs, zdarzen));
        System.out.println("Lącznie wydasz: " + o2.progressiveBetSpend(stawka, nstawka, kolejki));
        System.out.println("W ostatniej kolejce wydasz: " + o2.lastSpend(stawka, nstawka, kolejki));
        System.out.println("Wygrana wyniesie: " + o2.win());
        System.out.println("A zysk: " +  o2.profit());
        System.out.println("--------------------------------------");
    }

    private static void opt1(Option1 o1, Scanner cs) {
        System.out.println("Podaj stawkę: ");
        double bet = cs.nextDouble();
        System.out.println("Podaj średni kurs: ");
        double kurs = cs.nextDouble();
        System.out.println("Podaj ilość zdarzeń: ");
        int xzdarzen = cs.nextInt();
        System.out.println("Sredni kurs ze zdarzen to: " + o1.averageExchange(xzdarzen, kurs));
        System.out.println("W tym sezonie postawisz całkowicie: " + o1.seasonBet(36, bet));
        System.out.println("Wygrana na koniec sezonu wyniesie: " + o1.seasonWin());
        System.out.println("Jeśli trafisz 40% meczy zyskasz: " + o1.percentageChanceWin(0.4));
        System.out.println("Jeśli trafisz 50% meczy zyskasz: " + o1.percentageChanceWin(0.5));
        System.out.println("Jeśli trafisz 60% meczy zyskasz: " + o1.percentageChanceWin(0.6));
        System.out.println("Jeśli trafisz 70% meczy zyskasz: " + o1.percentageChanceWin(0.7));
        System.out.println("--------------------------------------");

    }

}
