package menu;

import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by RENT on 2017-07-28.
 */
public class Option3 {

    int kolejki, mecze;
    double chance;

    Random random = new Random();
    Set<Integer> set = new TreeSet<>();

    public double chance(int mecze) {
        double chance = (double) 1 / (Math.pow(3,mecze));
        return chance;
    }

    public Set<Integer> roundChance(int kolejki) {
        for (int i = 1; i <= random.nextInt(kolejki); i++) {
            int a = random.nextInt(kolejki) + 1;
            set.add(a);
        }
        return set;
    }


}
