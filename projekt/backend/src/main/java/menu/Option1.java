package menu;

/**
 * Created by RENT on 2017-07-28.
 */
public class Option1 {

    double stawka, kurs, skurs, zysk, wygrana, pchance, sstawka;
    int xzdarzen, games;

    public double averageExchange (int xzdarzen, double kurs) {
        skurs = Math.pow( kurs , xzdarzen);
        return skurs;
    }

    public double seasonBet (int games, double stawka) {
        sstawka = games*stawka;
        return sstawka;
    }

    public double seasonWin () {
        wygrana = skurs * sstawka;

        return wygrana;
    }

    public double percentageChanceWin ( double pchance) {
        zysk = (wygrana * pchance) - sstawka;
        return zysk*0.88;
    }
}
