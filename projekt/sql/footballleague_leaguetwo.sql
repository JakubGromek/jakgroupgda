-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: footballleague
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `leaguetwo`
--

DROP TABLE IF EXISTS `leaguetwo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leaguetwo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team` varchar(45) DEFAULT NULL,
  `draws` int(11) DEFAULT NULL,
  `season` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leaguetwo`
--

LOCK TABLES `leaguetwo` WRITE;
/*!40000 ALTER TABLE `leaguetwo` DISABLE KEYS */;
INSERT INTO `leaguetwo` VALUES (1,'Portsmouth',9,'16-17'),(2,'Plymouth',9,'16-17'),(3,'Doncaster',10,'16-17'),(4,'Luton',17,'16-17'),(5,'Exeter',8,'16-17'),(6,'Carlisle',17,'16-17'),(7,'Blackpool',16,'16-17'),(8,'Colchester',12,'16-17'),(9,'Wycombe',12,'16-17'),(10,'Stevenage',7,'16-17'),(11,'Cambridge Utd',9,'16-17'),(12,'Mansfield',15,'16-17'),(13,'Accrington',14,'16-17'),(14,'Grimsby Town',11,'16-17'),(15,'Barnet',15,'16-17'),(16,'Notts County',8,'16-17'),(17,'Crewe',13,'16-17'),(18,'Morecambe',10,'16-17'),(19,'Crawley Town',12,'16-17'),(20,'Yeovil',17,'16-17'),(21,'Cheltenham',14,'16-17'),(22,'Newport',12,'16-17'),(23,'Hartlepool',13,'16-17'),(24,'Leyton Orient',6,'16-17'),(25,'Northampton',12,'15-16'),(26,'Oxford Utd',14,'15-16'),(27,'Bristol Rovers',7,'15-16'),(28,'Accrington',13,'15-16'),(29,'Plymouth',9,'15-16'),(30,'Portsmouth',15,'15-16'),(31,'Wimbledon',12,'15-16'),(32,'Leyton Orient',12,'15-16'),(33,'Cambridge Utd',14,'15-16'),(34,'Carlisle',16,'15-16'),(35,'Luton',9,'15-16'),(36,'Mansfield',13,'15-16'),(37,'Wycombe',13,'15-16'),(38,'Exeter',13,'15-16'),(39,'Barnet',11,'15-16'),(40,'Hartlepool',6,'15-16'),(41,'Notts County',9,'15-16'),(42,'Stevenage',15,'15-16'),(43,'Yeovil',15,'15-16'),(44,'Crawley Town',8,'15-16'),(45,'Morecambe',10,'15-16'),(46,'Newport',13,'15-16'),(47,'Dagenham&Red.',10,'15-16'),(48,'York',13,'15-16'),(49,'Burton',10,'14-15'),(50,'Shrewsbury',8,'14-15'),(51,'Bury',7,'14-15'),(52,'Wycombe',15,'14-15'),(53,'Southend',12,'14-15'),(54,'Stevenage',12,'14-15'),(55,'Plymouth',11,'14-15'),(56,'Luton',11,'14-15'),(57,'Newport',11,'14-15'),(58,'Exeter',13,'14-15'),(59,'Morecambe',12,'14-15'),(60,'Northampton',7,'14-15'),(61,'Oxford Utd',16,'14-15'),(62,'Dagenham&Red.',8,'14-15'),(63,'Wimbledon',16,'14-15'),(64,'Portsmouth',15,'14-15'),(65,'Accrington',11,'14-15'),(66,'York',19,'14-15'),(67,'Cambridge Utd',12,'14-15'),(68,'Carlisle',8,'14-15'),(69,'Mansfield',9,'14-15'),(70,'Hartlepool',9,'14-15'),(71,'Cheltenham',14,'14-15'),(72,'Tranmere',12,'14-15');
/*!40000 ALTER TABLE `leaguetwo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-08 12:37:52
