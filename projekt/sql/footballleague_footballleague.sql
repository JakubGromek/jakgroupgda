-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: footballleague
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `footballleague`
--

DROP TABLE IF EXISTS `footballleague`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `footballleague` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `draws` int(11) DEFAULT NULL,
  `team` varchar(255) DEFAULT NULL,
  `season` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `footballleague`
--

LOCK TABLES `footballleague` WRITE;
/*!40000 ALTER TABLE `footballleague` DISABLE KEYS */;
INSERT INTO `footballleague` VALUES (1,3,'Chelsea','16-17'),(2,8,'Tottenham','16-17'),(3,9,'Manchester City','16-17'),(4,10,'Loverpool','16-17'),(5,6,'Arsenal','16-17'),(6,15,'Manchester United','16-17'),(7,10,'Everton','16-17'),(8,10,'Southampton','16-17'),(9,10,'Bournemouth','16-17'),(10,9,'West Brom','16-17'),(11,9,'West Ham','16-17'),(12,8,'Leicester','16-17'),(13,11,'Stoke','16-17'),(14,5,'Crystal Palace','16-17'),(15,5,'Swansea','16-17'),(16,7,'Burnley','16-17'),(17,7,'Watford','16-17'),(18,7,'Hull','16-17'),(19,13,'Middlesbrough','16-17'),(20,6,'Sunderland','16-17'),(21,12,'Leicester','15-16'),(22,11,'Arsenal','15-16'),(23,13,'Tottenham','15-16'),(24,9,'Manchester City','15-16'),(25,9,'Manchester United','15-16'),(26,9,'Southampton','15-16'),(27,14,'West Ham','15-16'),(28,12,'Liverpool','15-16'),(29,10,'Stoke','15-16'),(30,14,'Chelsea','15-16'),(31,14,'Everton','15-16'),(32,11,'Swansea','15-16'),(33,9,'Watford','15-16'),(34,13,'West Brom','15-16'),(35,9,'Crystal Palace','15-16'),(36,9,'Bournemouth','15-16'),(37,12,'Sunderland','15-16'),(38,10,'Newcastle','15-16'),(39,7,'Norwich','15-16'),(40,8,'Aston Villa','15-16'),(41,9,'Chelsea','14-15'),(42,7,'Manchester City','14-15'),(43,9,'Arsenal','14-15'),(44,10,'Manchester United','14-15'),(45,7,'Tottenham','14-15'),(46,8,'Liverpool','14-15'),(47,6,'Southampton','14-15'),(48,8,'Swansea','14-15'),(49,9,'Stoke','14-15'),(50,9,'Crystal Palace','14-15'),(51,11,'Everton','14-15'),(52,11,'West Ham','14-15'),(53,11,'West Brom','14-15'),(54,8,'Leicester','14-15'),(55,9,'Newcastle','14-15'),(56,17,'Sunderland','14-15'),(57,8,'Aston Villa','14-15'),(58,11,'Hull','14-15'),(59,12,'Burnley','14-15'),(60,6,'QPR','14-15');
/*!40000 ALTER TABLE `footballleague` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-04 14:31:41
