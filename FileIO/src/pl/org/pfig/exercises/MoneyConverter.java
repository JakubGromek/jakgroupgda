package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MoneyConverter {
	
	private final String filename = "resources/currency.txt";
	
	public double readCourse(String currency) {
		File f = new File(filename);
		currency = currency.toUpperCase();
		
		
		try {
			//String money = "100";
			String currentLine = "";
			Scanner sc = new Scanner(f);
			while(sc.hasNextLine()){
				currentLine = sc.nextLine();
				String[] temp = currentLine.split("\t");
				String[] values = temp[0].split(" ");
				if(values[1].equalsIgnoreCase(currency)) {
					//return (Double.parseDouble(money))/Double.parseDouble(temp[1].replace(",", "."));
					return Double.parseDouble(temp[1].replace(",", "."));
				}
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		return 0;
	}
	
	public double convert (double money, String currency) {
		System.out.println("\t"+this.readCourse(currency));
		return money / this.readCourse(currency);
	}
	
	public double convert(double money, String to, String from) {
		return (money*this.readCourse(from))/this.readCourse(to);
	}

}
