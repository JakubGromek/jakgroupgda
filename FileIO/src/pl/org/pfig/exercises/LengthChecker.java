package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class LengthChecker {
//	private final String path = "resources/";
//public String readFile(String filename){
//		String words = new String(filename);
//		File f = new File(filename);
//		
//		try {
//			Scanner sc = new Scanner(f);
//			while (sc.hasNextLine()) {
//				sc.nextLine();
//				
//			}
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//		
//		return words;
//		
//	}
	
private final String filename = "resources/words.txt";

private boolean isProperLength(String arg, int len){
	return arg.length() > len;
}
	
	public String readFile(String filename) {
		File f = new File("resources/"+filename);
		String sentence ="";
		
		try {
			Scanner sc= new Scanner(f);
			
			while(sc.hasNextLine()) {
				sentence += sc.nextLine() + "\r";
				
			}
			
			sc.close();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
		return sentence;
	}
	
	public String[] readFile2(String filename) {
		File f = new File("resources/"+filename);
		String[] ret = new String[countLines(filename)];
		int i =0;
		
		try (Scanner sc= new Scanner(f)) {
			for (int ii=0; sc.hasNextLine(); i++)
				
			
			
			while(sc.hasNextLine()) {
				ret[i++] = sc.nextLine();
				
			}
			
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
		return ret;
	}
	public int countLines(String filename){
		File f = new File(filename);
		int lines = 0;
		try {
			Scanner sc = new Scanner(f);
			while (sc.hasNextLine()) {
				lines++;
				sc.nextLine();

			}
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
		
		return lines;
		
	}
	
	private void writeFile(String[] fileContent, int len) {
				try {
					String path = "resources/";
					FileOutputStream fos = new FileOutputStream(path  + "words_" + len + ".txt");
					PrintWriter pw = new PrintWriter(fos);
					for(String line : fileContent) {
						if(isProperLength(line, len)) {
							pw.println(line);
						}
					}
					pw.close();
				} catch (FileNotFoundException e) {
					System.out.println(e.getMessage());
				}
			}
			
			//+make(String fileInput, int len):void
			
			public void make(String fileInput, int len) {
				String[] fileContent = readFile2(fileInput);
				writeFile(fileContent, len);
			}

	
	
	}


