package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

public class Columner {
	// �wiczenie 8
	// Napisz klas� Columner zawieraj�c� metody i pola:
	// -filename:String
	// -currentColumn:double[]
	// +Columner
	// +Columner(filename:String)
	// +sumColumn(column:int):double
	// +avgColumn(column:int):double
	// +countColumn(column:int):int
	// +maxColumn(column:int):double
	// +minColumn(column:int):double
	// -readFile():void
	// +writeColumn(filename:String):void
	// +readColumn(column:int)
	// Metoda readColumn() powinna odczytywa� warto�ci aktualnej kolumny do pola
	// currentColumn. Metody *Column powinny zwraca� sum�, �redni� element�w
	// oraz ich ilo��,
	// maksymalny i minimalny element.
	// Metoda writeColumn() powinna zapisywa� warto�ci wybranej kolumny do
	// pliku. W przypadku
	// braku danej kolumny (ilo�� kolumn w pliku mniejsza ni� przekazany
	// argument) wyrzu� wyj�tek
	// IllegalArgumentException.
	private String filename;
	private double[] currentColumn;
	private final String path = "resources/";
	private LinkedList<String> fileContent = new LinkedList<>();

	public Columner() {

	}

	public Columner(String filename) {
		this.filename = filename;
	}

	private void readFile() {
		if (fileContent.size() == 0) {
			File f = new File(path + filename);
			// LinkedList<String> fileContent = new LinkedList<>();
			try {
				Scanner sc = new Scanner(f);
				// String currentline;
				while (sc.hasNextLine()) {
					// currentline = sc.nextLine();
					fileContent.add(sc.nextLine());
				}
				sc.close();
			} catch (FileNotFoundException e) {

				System.out.println(e.getMessage());
			}
		}
	}

	public double[] readColumn(int column) throws WrongColumnException {

		readFile();
		int maxColNum = fileContent.get(0).split("\t").length;
		if (column >= 1 && column <= maxColNum) {
			currentColumn = new double[fileContent.size()];
			int i =0;
			for ( String line : fileContent) {
				String[] cols = line.split("\t");
				currentColumn[i++] = Double.parseDouble(cols[column -1].replace(",", ".")); 
			}
		} else {
			throw new WrongColumnException("Bad column value");
		}
		return currentColumn;
	}

	public double sumColumn(int column) throws WrongColumnException {
		//readColumn(column);
		double sum = 0;
		for(double d : readColumn(column)){
			sum +=d;
		}
		return sum;

	}

	public double avgColumn(int column) throws WrongColumnException {
		double avg = 0;
		double sum = 0;
		for (double d : readColumn(column)){
			sum += d;
			avg = sum / fileContent.size();
		}
		return avg;

	}

	public double countColumn(int column) {
		readFile();
		return fileContent.get(0).split("\t").length;

	}

	public double maxColumn(int column) throws WrongColumnException {
		//readFile();
		double max = readColumn(column)[0];
		
		for (int i =1; i< readColumn(column).length; i++ ){
			max=Math.max(max, readColumn(column)[i]);
		}
		//Math.max(max, readColumn(column).length);
		
		return max;

	}

	public double minColumn(int column) throws WrongColumnException {
	readFile();
	double min = readColumn(column)[0];
	
	for (int i =1; i < readColumn(column).length; i++ ){
		min=Math.min(min, readColumn(column)[i]);
	}
	//Math.max(max, readColumn(column).length);
	
	return min;

	}

}
