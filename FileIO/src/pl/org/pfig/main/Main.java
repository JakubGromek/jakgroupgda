package pl.org.pfig.main;

import java.util.Arrays;
import java.util.List;

import pl.org.pfig.exercises.AvgChecker;
import pl.org.pfig.exercises.Columner;
import pl.org.pfig.exercises.LengthChecker;
import pl.org.pfig.exercises.MoneyConverter;
import pl.org.pfig.exercises.Sentence;
import pl.org.pfig.exercises.Sentencer;
import pl.org.pfig.exercises.TempConverter;
import pl.org.pfig.exercises.WrongColumnException;

public class Main {



	public static void main(String[] args) {
		
		Columner c = new Columner("data.txt");
		try {
		System.out.println(c.sumColumn(2));
		System.out.println(c.avgColumn(2));
		System.out.println(c.countColumn(2));
		System.out.println(c.maxColumn(2));
		System.out.println(c.minColumn(2));
		} catch (WrongColumnException e){
			System.out.println(e.getMessage());
		}
		
		
//		
//		LengthChecker lc = new LengthChecker();
//		String mySentence = lc.readFile("words.txt");
//		System.out.println(mySentence);
//		System.out.println(lc.countLines("words.txt"));
//		//lc.make("words.txt", 8);
		

		
//		TempConverter tK = new TempConverter();
//		double[] tempC = tK.readTemp("tempC.txt");
		
//		for (double d : tempC) {
//			System.out.println("Temp: " +d);
//		}
		//tK.writeTemp(tempC);
		
		//System.out.println(tc.readTemp("tempC.txt"));
		//System.out.println(tc.countLines("tempC.txt"));
		
//		MoneyConverter mc = new MoneyConverter();
//		MoneyConverter m = new MoneyConverter();
//		System.out.println(m.convert(100, "USD"));
//		System.out.println(m.convert(100, "ILS", "USD"));
//		  
//		
//		List<String> currencies = Arrays.asList("USD","EUR");
//		
//		for(String c : currencies) {
//			System.out.println("Wymiana 100 PLN na "+c+ " | "+ mc.readCourse(c));
//		}

//		Sentencer s = new Sentencer();
//		
//		String mySentence = s.readSentence("test.txt");
//		s.writeSentence("mySentence.txt", mySentence);
//		
//		String myStr = "1 CNY	0.6064";
//		String[] temp = myStr.split("\t");
//		String[] values = temp[0].split(" ");
//		
//		int howMany = Integer.parseInt(values[0]);
//		String currency = values[1];
//		double course = Double.parseDouble(temp[1].replace(",", "."));
//		
//		System.out.println(howMany+ " | "+currency+" | "+course);
	}

}
