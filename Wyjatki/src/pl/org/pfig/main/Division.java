package pl.org.pfig.main;

public class Division {
//	Napisz klas� Division, kt�ra b�dzie posiada�a metody:
//		public static double divide(int a, int b);
//		public static double divide(double a, double b);
//		Metoda divide() powinna zwraca� IllegalArgumentException w przypadku podania 0
//		jako dzieln� (zmienna b). Poinformuj kompilator o zwracanym b��dzie.
	
	public static double divide(int a, int b) throws IllegalArgumentException {
		if (b > 0) {
			System.out.println("Wynik dzielenia "+a+" i "+b+" to: ");
		}

		else if (b == 0) {
			throw new IllegalArgumentException("B jest r�wne 0");
		} 
		return  a/b;
		
	}
	public static double divide(double a, double b){
		double doubleDiv = a/b;
		return doubleDiv;
		
	}

}
