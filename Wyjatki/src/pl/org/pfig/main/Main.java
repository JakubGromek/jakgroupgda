package pl.org.pfig.main;

import java.util.Scanner;

public class Main {

	// private static Scanner cs;

	public static void main(String[] args) throws IllegalArgumentException {
		


		ReadNumbers rn = new ReadNumbers();
		System.out.println(rn.readDouble());

		Division esc1 = new Division();
		try {
			System.out.println(Division.divide(4, 0));
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			System.out.println("wystapil wyjatek: " + e.getMessage());
		}

		Square esc = new Square();
		try {

			System.out.println(esc.square(-2));
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		int[] array = new int[10];

		Scanner cs = new Scanner(System.in);
		System.out.println("Wczytaj 10 liczb do tablicy:");
		for (int i = 0; i < 10; i++) {
			array[i] = cs.nextInt();
		}
		try {
			System.out.println(array[10]);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Brak elementu pod indeksem: " + e.getMessage());
		}

//		ExceptionSimpleClass esc = new ExceptionSimpleClass();
//		try {
//			esc.exceptionExample("kuba");
//		} catch (KubaException e) {
//			System.out.println(e.getMessage());
//		}
//
//		try {
//			esc.make(55);
//		} catch (IllegalArgumentException e) {
//			e.printStackTrace();
//			// System.out.println("[Error]" + e.getMessage());
//
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//		} finally {
//			System.out.println("[END]");
//		}

	}

}
