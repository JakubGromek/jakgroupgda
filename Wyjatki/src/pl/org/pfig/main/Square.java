package pl.org.pfig.main;

public class Square {
	//
	// Utw�rz klas� Square zawieraj�c� metod�:
	// public static double square(int n);
	// obliczaj�c� pierwiastek z liczby n. Wykorzystaj do oblicze� metod�
	// Math.sqrt(); Je�eli jednak
	// u�ytkownik przeka�e liczb� mniejsz� od 0 zwr�� wyj�tek
	// IllegalArgumentException.

	public static double square(int n) throws IllegalArgumentException{
		double square =  Math.sqrt(n);
		if (n > 0) {
			System.out.print("Pierwiastek z " + n + " to: ");
		}

		else if (n<0) {
			throw new IllegalArgumentException("N jest mniejsze od 0");
		} 
		
		return square;
	}

}
