package pl.org.pfig.main;

import java.util.InputMismatchException;
import java.util.Scanner;

public class QuadraticEquation {
	// Napisz klas� QuadraticEquation posiadaj�c� konstruktor bez i
	// sparametryzowany dla trzech
	// p�l:
	// private int a;
	// private int b;
	// private int c;
	// oraz metody:
	// private int getNumber(); - wczytuj�c� liczb� ca�kowit� od u�ytkownika
	// public double[2] solve(); - rozwi�zuj�c� r�wnanie kwadratowe
	// Metoda solve() w przypadku braku zainicjalizowania zmiennych a, b, c (lub
	// podania 0 dla
	// wszystkich trzech parametr�w) powinna wywo�a� metod� getNumber() przed
	// dokonaniem
	// oblicze�. Metoda powinna zwraca� tablic� z rozwi�zaniami [x1, x2] b�d�
	// [x1, x1] i zg�asza�
	// wyj�tek ArithmeticException w przypadku ujemnej delty. Zwr�� uwag� na
	// przypadek, kiedy
	// a = 0, b !=0, c != 0.
	// Obs�u� wyj�tek w metodzie main().

	private int a = 0;

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public int getC() {
		return c;
	}

	public void setC(int c) {
		this.c = c;
	}

	private int b = 0;
	private int c = 0;

	private int getNumber() {
		int in;
		Scanner cs = new Scanner(System.in);
		System.out.println("Podaj liczbe calkowita: ");
		try {
			in = cs.nextInt();
		} catch (InputMismatchException e) {
			in = 0;
		}
		return in;

	}

	public double[] solve() throws ArithmeticException {

		double x1, x2, delta;
		
		while ((a==0)&&(b==0)&&(c==0)) {
		a = getNumber();
		b = getNumber();
		c = getNumber();
		}
		
		delta = b*b-4*a*c;
		if(delta >= 0){
		x1 = (Math.sqrt(delta)-b) / (2*a);
		x2 = (-Math.sqrt(delta)-b) / (2*a);
		} else {
			throw new ArithmeticException("Delta ujemna!");
		}
		System.out.println("x1="+x1+", x2=" + x2 +", delta=" + delta);
		return new double[] {x1,x2};
	}

}
