<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<c:import url="header.jsp"/>

<section>
    <h2>Dodaj film</h2>
    <form action="MovieServlet" method="post">
        <label for="movieName">Podaj nazwę filmu:</label>
        <input type="text" name="movieName" id="movieName" value="<c:out value="${movie.name}" />"/>
        <input type="hidden" name="action" value="edit"/>
        <input type="hidden" name="id" value="<c:out value="${movie.id}" />" />
        <button type="submit">Dodaj</button>
    </form>
</section>
<c:import url="footer.jsp"/>
