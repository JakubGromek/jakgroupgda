<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<c:import url="header.jsp"/>
<section><h1>
    Filmy:
</h1>
    <%--<form action="MovieServlet" method="get">--%>
    <table border="1" cellpadding="2" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nazwa</th>
            <th>Akcje</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${movies}" var="movie">
            <tr>

                <td><c:out value="${movie.id}"/>.</td>
                <td>
                    <p><c:out value="${movie.name}"/></p>
                </td>
                <td><a href="/MovieServlet?action=edit&id=<c:out value="${movie.id}" />"> Edytuj</a> &bull;
                    <a href="/MovieServlet?action=delete&id=<c:out value="${movie.id}" />"> Usuń</a> </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <%--</form>--%>
</section>
<c:import url="footer.jsp"/>