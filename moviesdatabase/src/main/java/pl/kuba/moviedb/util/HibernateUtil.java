package pl.kuba.moviedb.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;



/**
 * Created by RENT on 2017-07-24.
 */
public class HibernateUtil {
    private static final SessionFactory sf = new Configuration().configure().buildSessionFactory();

    private HibernateUtil() {}

    public static Session openSession() {
        return sf.openSession();
    }
}
