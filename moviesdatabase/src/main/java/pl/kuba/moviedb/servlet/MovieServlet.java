package pl.kuba.moviedb.servlet;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.kuba.moviedb.entity.Movie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * Created by RENT on 2017-07-24.
 */

@WebServlet(name = "MovieServlet")
public class MovieServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        Session session = pl.kuba.moviedb.util.HibernateUtil.openSession();
        if (action.equals("add")) {
            String name = request.getParameter("movieName");

            Movie m = new Movie(name);
            Transaction t = session.beginTransaction();
            session.save(m);
            t.commit();


        } else if (action.equals("edit")){
            String name = request.getParameter("movieName");
            int id = Integer.valueOf(request.getParameter("id"));
            Movie m = new Movie();
            m.setId(id);
            Transaction t = session.beginTransaction();
            session.update(m);
            t.commit();
        }
       // request.getRequestDispatcher("MovieServlet?action=show").forward(request,response);
        response.sendRedirect("MovieServlet?action=show");
        session.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            Session session = pl.kuba.moviedb.util.HibernateUtil.openSession();
            String action = request.getParameter("action");

            if (action.equals("show")) {
                Transaction t = session.beginTransaction();
                List<Movie> movies = session.createQuery("from Movie where id > 0").list();
                t.commit();
                request.setAttribute("movies", movies);
                request.getRequestDispatcher("movie.jsp").forward(request, response);
            } else if (action.equals("edit")){
                int id = Integer.parseInt(request.getParameter("id"));
                Movie m = session.get(Movie.class, id);
                request.setAttribute("movie", m);
                request.getRequestDispatcher("edit.jsp").forward(request,response);

            } else if (action.equals("delete")){

                Transaction t = session.beginTransaction();
                Movie m = new Movie();
                m.setId(Integer.parseInt(request.getParameter("id")));
                session.delete(m);
                t.commit();
                request.getRequestDispatcher("MovieServlet?action=show").forward(request,response);
            }

        session.close();
    }
}
