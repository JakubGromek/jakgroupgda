package pl.kuba.zoo.entity;

import javax.persistence.*;

/**
 * Created by RENT on 2017-07-12.
 */
@Entity
@Table (name = "staff")
public class Staff {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column (name = "lastname")
    private String lastname;
    @Column (name = "age")
    private int age;

    public Staff(){}

    public Staff(String name, String lastname, int age) {
        this.name = name;
        this.lastname = lastname;
        this.age = age;
    }

    public Staff(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public Staff setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Staff setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public Staff setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public int getAge() {
        return age;
    }

    public Staff setAge(int age) {
        this.age = age;
        return this;
    }
}
