package pl.kuba.zoo.entity;

/**
 * Created by RENT on 2017-07-12.
 */

import javax.persistence.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="animal")
public class Animal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="name")
    private String name;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AnimalFeed> animalFeedSet = new LinkedList<AnimalFeed>();

    public Animal() {
    }

    public Animal(String name){
        this.name = name;
    }

    public Animal(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Animal setName(String name) {
        this.name = name;
        return this;
    }

    public int getId() {
        return id;
    }

    public Animal setId(int id) {
        this.id = id;
        return this;
    }

    public List<AnimalFeed> getAnimalFeedSet() {
        return animalFeedSet;
    }

    public Animal setAnimalFeedSet(List<AnimalFeed> animalFeedSet) {
        this.animalFeedSet = animalFeedSet;
        return this;
    }
}
