package pl.kuba.zoo.entity;

import javax.persistence.*;

/**
 * Created by RENT on 2017-07-13.
 */
@Entity

public class AnimalFeed {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column
    private String who;

    @Column
    private int amount;

    @ManyToOne(fetch = FetchType.LAZY)
    private Animal animal;

    public int getId() {
        return id;
    }

    public AnimalFeed setId(int id) {
        this.id = id;
        return this;
    }

    public String getWho() {
        return who;
    }

    public AnimalFeed setWho(String who) {
        this.who = who;
        return this;
    }

    public int getAmount() {
        return amount;
    }

    public AnimalFeed setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public Animal getAnimal() {
        return animal;
    }

    public AnimalFeed setAnimal(Animal animal) {
        this.animal = animal;
        return this;
    }

    public AnimalFeed(String who, int amount, Animal animal) {
        this.who = who;
        this.amount = amount;
        this.animal = animal;


    }
}
