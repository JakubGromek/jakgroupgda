package pl.kuba.zoo.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.kuba.zoo.entity.Animal;
import pl.kuba.zoo.entity.AnimalFeed;
import pl.kuba.zoo.util.HibernateUtil;

import java.util.List;

/**
 * Created by RENT on 2017-07-13.
 */
public class AnimalFeedDAO implements AbstractDAO<AnimalFeed> {

    public boolean insert(AnimalFeed type) {
                Session session = HibernateUtil.openSession();
                Transaction t = session.beginTransaction();
                int newId = Integer.valueOf(session.save(type) + "");
                System.out.println("Umieściłem rekord o ID = " + newId);
                t.commit();
                session.close();
                return true;
            }

            public boolean delete(AnimalFeed type) {
                Session session = HibernateUtil.openSession();
                Transaction t = session.beginTransaction();
                session.delete(type);
                t.commit();
                if(this.get(type.getId()) == null) {
                    return true;
                }
                session.close();
                return false;
            }
            public boolean delete(int id) {
                Session session = HibernateUtil.openSession();
                Transaction t = session.beginTransaction();
                session.delete(this.get(id));
                t.commit();
                session.close();
                return true;
            }

            public boolean update(AnimalFeed type) {
                Session session = HibernateUtil.openSession();
                Transaction t = session.beginTransaction();
                session.update(type);
                t.commit();
                session.close();
                return true;
            }

            public AnimalFeed get(int id) {
                AnimalFeed animal;
                Session session = HibernateUtil.openSession();
                animal = session.load(AnimalFeed.class, id);
                session.close();
                return animal;
            }
            public List<AnimalFeed> get() {
                List<AnimalFeed> animals;
                Session session = HibernateUtil.openSession();
                animals = session.createQuery("from AnimalFeed").list();
                session.close();
                return animals;
            }
}
