package pl.kuba.zoo.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.kuba.zoo.entity.Staff;
import pl.kuba.zoo.util.HibernateUtil;

import java.util.List;

/**
 * Created by RENT on 2017-07-12.
 */
public class StaffDAO implements AbstractDAO<Staff> {
    public boolean insert(Staff type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.save(type);
        t.commit();
        session.close();
        return true;
    }

    public boolean delete(Staff type) {
        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(type);
        t.commit();
        if (this.get(type.getId()) == null) {
            return true;
        }
        session.close();
        return false;
    }

    public boolean delete(int id) {

        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.delete(this.get(id));
        t.commit();
        session.close();
        return true;
    }

    public boolean update(Staff type) {

        Session session = HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.update(type);
        t.commit();
        session.close();
        return false;
    }

    public Staff get(int id) {
        Staff animal;
        Session session = HibernateUtil.openSession();
        animal = session.load(Staff.class, id);
        session.close();
        return animal;
    }

    public List<Staff> get() {
        List<Staff> staff;
        Session session = HibernateUtil.openSession();

        staff = session.createQuery("from Staff").list();
        session.close();
        return staff;
    }
}
