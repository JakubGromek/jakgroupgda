package pl.kuba.zoo;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import pl.kuba.zoo.dao.AnimalDAO;
import pl.kuba.zoo.dao.AnimalFeedDAO;
import pl.kuba.zoo.dao.StaffDAO;
import pl.kuba.zoo.entity.Animal;
import pl.kuba.zoo.entity.AnimalFeed;
import pl.kuba.zoo.entity.Staff;
import pl.kuba.zoo.util.HibernateUtil;

import java.util.List;

/**
 * Created by RENT on 2017-07-12.
 */
public class Main {
    public static void main(String[] args) {
//        StaffDAO staffDAO = new StaffDAO();
//        Staff s = new Staff();
//        s.setName("Janek");
//        s.setLastname("Wiśniewski");
//        s.setAge(334);
//        staffDAO.insert(s);
//        staffDAO.insert(new Staff("Mieszko","I", 1024));
//        staffDAO.insert(new Staff("Stefan","Batory", 666));
//
//        List<Staff> staff = new StaffDAO().get();
//        for (Staff st : staff){
//            System.out.println(st.getId()+". "+st.getName()+" "+st.getLastname()+", "+st.getAge()+" lat(a).\n");
//        }

        AnimalDAO animalDAO = new AnimalDAO();
        AnimalFeedDAO animalFeedDAO = new AnimalFeedDAO();

        Animal a = new Animal();
        a.setName("Słoń");
        animalDAO.insert(a);

        AnimalFeed af = new AnimalFeed("Kuba", 8, a);
        animalFeedDAO.insert(af);
        animalFeedDAO.insert(new AnimalFeed("Karolina", 3, new Animal("Krokodyl")));
        animalFeedDAO.insert(new AnimalFeed("Michał", 5, a));

        System.out.println("Nakarmiono: " + animalFeedDAO.get(1).getAnimal().getName());


//        // Otworzenie sesji Hibernate
//        Session session = HibernateUtil.openSession();
//        //BEGIN : create
//        Transaction t = session.beginTransaction();
//        Animal a = new Animal();
//        a.setName("Słoń");
//        session.save(a);
//        session.save(new Animal("Żyrafa"));
//        session.save(new Animal("Mrówkojad"));
//        session.save(new Animal("Wilk"));
//        t.commit();
//        session.close();
//        //END: create
//        Animal a = new Animal();
//        a.setName("Słoń");
//
//        animalDAO.insert(a);
//        animalDAO.insert(new Animal("Orzzeł"));
//        animalDAO.insert(new Animal("Gołąb"));


        // BEGIN: retrieve
//         session = HibernateUtil.openSession();
//        Animal animal = null;
//        t = session.beginTransaction();
//        String query = "from Animal where id = :id";
//        Query q = session.createQuery(query);
//        q.setParameter("id", 1);
//        animal = (Animal) q.uniqueResult();
//        System.out.println(animal.getId() + " | " + animal.getName());
//        t.commit();
//        session.close();
        // END: retrieve
//        Animal slon = new Animal();
//        slon = animalDAO.get(1);
//        System.out.println("Pobrales id = "+slon.getId()+" " +slon.getName());

        // BEGIN : update
//         session = HibernateUtil.openSession();
//        t = session.beginTransaction();
//        animal.setName("Krokodyl");
//        session.update(animal);
//        t.commit();
//        session.close();
        //END : update
//        slon.setName("Nowy Słoń");
//        animalDAO.update(slon);

        // BEGIN: delete
//         session = HibernateUtil.openSession();
//
//        t = session.beginTransaction();
//        Animal animalToDelete = session.load(Animal.class, new Integer(2));
//        session.delete(animalToDelete);
//        t.commit();
//        session.close();
        // END : delete
//
//        animalDAO.delete(2);
//
//        Animal secAnimal = new Animal();


        //BEGIN: retrieve all
//         session = HibernateUtil.openSession();
//
//        List<Animal> animals = session.createQuery("from Animal").list();
//        for (Animal anim : animals){
//            System.out.println(anim.getId()+ " -- "+ anim.getName());
//        }
//        //END: retrieve all
//
//        // Zamknięcie sesji Hibernate
//        session.close();

        //AnimalDAO animalDAO = new AnimalDAO();
//        List<Animal> animals = animalDAO.get();
//        for (Animal anim : animals){
//            System.out.println(anim.getId()+". "+anim.getName());
//        }

    }
}
