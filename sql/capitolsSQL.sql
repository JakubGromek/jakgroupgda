create database states_citys;

/*state (id,name,population,capital_id)
city (id,name,citizens)
 
Wstaw do bazy 5 europejskich państw i ich stolic oraz 5 miast nie będących stolicami.
Wyświetl pary (state_name, capital_name)
Wyświetl tylko miasta będące stolicami.
Wyświetl pary (state_name, capital_citizens) 
 capital_citizens - ilość mieszkańców stolicy / ilość obywateli
Wyświetl państwa w których powyżej 10% mieszkańców mieszka w stolicy.
Dodaj pole is_capital do tabeli city i ustaw prawidłową wartość dla każdego miasta.
Usuń państwa nie będące stolicami.*/

create table state (
id int auto_increment primary key,
name varchar(12),
population int,
capital_id int
);

create table city (
id int auto_increment primary key,
name varchar(12),
citizens int);

insert into city(name, citizens) values
("Warszawa", 4000000),
("Praga", 2934711),
("Berlin", 8983393),
("Londyn", 30001202),
("Budapeszt", 3000000),
("Gdańsk", 800000),
("Pilzno", 300000),
("Monachium", 6000000),
("Manchester", 12000000),
("Debreczyn", 700000);

insert into state (name, population, capital_id) values
("Polska", 40000000, 1),
("Czechy", 25000000, 2),
("Niemcy", 60000000, 3),
("Anglia", 70000000,4 ),
("Węgry", 20000000, 5);

select s.name, c.name from state s  left join city c on s.capital_id=c.id; /* państwo - stolica */
select c.name from state s left join city c on s.capital_id=c.id; /* tylko stolice*/
select s.name, c.citizens from state s left join city c on s.capital_id=c.id; /*mieszkancy stolic danego kraju*/
select s.name, s.population, c.citizens as mieszkancy_stolicy from state s left join city c on s.capital_id=c.id;

select s.name from state s left join city c on s.capital_id=c.id where c.citizens > s.population/10;

alter table city add is_capitol boolean;
select * from city;
ALTER TABLE city
MODIFY COLUMN is_capitol varchar(6);

update city set is_capitol = "Yes" where id IN (select capital_id from state);
SET SQL_SAFE_UPDATES = 0;
update city set is_capitol = "No" where is_capitol is null;



