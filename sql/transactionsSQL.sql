create database bank;	
use bank;

create table account(
id int auto_increment primary key,
number bigint,
balance int);

insert into account (number, balance) values (1234567890, 100), (0987654321, null);
select * from account;
start transaction;
update account set  balance=balance-100 where id=2;
update account set balance =balance +100 where id=1;
select * from account;
rollback;