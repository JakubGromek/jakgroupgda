create database shop;

create table product(
id int auto_increment primary key,
name varchar(20),
price double,
category_id int
);

alter table product add foreign key (category_id) references category(id);

ALTER TABLE product
MODIFY COLUMN price decimal(6,2);

create table category(
id int auto_increment primary key,
name varchar(20),
description varchar(44)
);

insert into category(name, description) values
("Słodycze", "Lody, cukierki ,batony"),
("Nabiał" , "Mleko, sery, jogurty"),
("Alkohole", "Piwa, wina, wódki, likiery");

insert into product(name,price,category_id) values
("Czekolada", 3.99, 1),
("Lody Kaktus", 1.59, 1),
("Krówki", 4.99, 1),
("Mleko Łaciate", 2.49, 2),
("Ser Gouda", 3.99, 2),
("Jogurt Jogobella", 0.99, 2),
("Tyskie", 2.49, 3),
("Specjal", 2.19, 3),
("Krupnik", 19.99, 3),
("Vapolicella", 59.99, 3);

select p.name from product p;
select c.name from category c;



select p.name, c.name from product p left join category c on p.category_id = c.id;

SELECT count(*), c.name from product p left join category c on p.category_id = c.id group by category_id;
SELECT avg(price), c.name from product p left join category c on p.category_id = c.id group by category_id;

create view product_full3 as select p.id,p.name as product_name ,price, c.name as category_name 
from product p left join category c on p.category_id=c.id;

select * from product_full3;

/*Do bazy shop dodaj tabelę:
customer (id,first_name,last_name)
order (id, date, paid, customer_id)
paid - enum ("NEW", "PAID", "CANCELED", "COMPLETION")
order_products (order_id, product_id,quantity)
 
Dodaj jednego klienta
Dodaj 2 zamówienia składające się z minimum 5 produktów.
Wyświetl 1 zamówienie w formacie (product_name, quantity)
Wyświetl zamówienia w formacie (products_quantity, data, customer, paid, amount)
products_quantity - suma wszystkich produktów
customer - imię i nazwisko klienta
amount - kwota do zapłaty*/

create table customer(
id int auto_increment primary key,
first_name varchar(12),
last_name varchar(12)
);
create table `order`(
id int auto_increment primary key,
 date date,
 customer_id int,
 paid enum('New', 'Paid', 'Canceled','Completion')
 );
 create table order_products(order_id int, product_id int, quantity int);
 
 insert into customer (first_name, last_name) values ("Jan", "Kowalski");
 insert into `order` (date, customer_id, paid) values 
 ('2017-07-03', 1, 'New'),
 ('2017-07-02', 1, 'Paid');
 insert into order_products(order_id, product_id, quantity) values
 (1, 27, 24),  (1, 28, 24), (1, 29, 5),(1, 30, 3),(1, 25, 5),
 (2, 21, 10),(2, 23, 10),(2, 26, 10),(2, 22, 10),(2, 24, 10);
 
 select p.name as product_name,op.quantity, op.quantity*price as amount,concat(c.first_name," ",c.last_name) as customer, date, `order`.paid
 from customer c, product p 
 right join order_products op 
 on p.id=op.product_id
 right join `order` 
 on `order`.id = op.order_id; /*where o.order_id = 1;*/
 
 
  select sum(op.quantity), sum(op.quantity*price) as amount,concat(c.first_name," ",c.last_name) as customer, date, `order`.paid
 from customer c, product p 
 right join order_products op 
 on p.id=op.product_id
 right join `order` 
 on `order`.id = op.order_id group by `order`.paid; /*where o.order_id = 1;*/
 

 
 select p.name as product_name, o.quantity from product p right join order_products op on p.id=op.product_id where o.order_id = 2;
 
 

