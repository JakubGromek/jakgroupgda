create database company;
drop database if exists company;
CREATE TABLE worker (
id int auto_increment primary key,
name varchar(32),
salary int ,
age int 
);

insert into worker (name,salary,age) values
("Andrzej",1000, 44), ("Janusz",1000,54),
 ("Grażyna",1000,67),
("Bonifacy", 1500, 32),
 ("Stefan", 1500, 36),
 ("Jadwiga",1500,41),
 ("Jan",2000,40),
 ("Anna",2000,29),
 ("Kuba",4000,29),
 ("Klaudia",4000,22);
 
 select * from worker;
 select salary from worker where salary = (select max(salary) from worker); 
  select salary from worker where salary = (select min(salary) from worker); 
   select min(salary), max(salary), avg(salary) from worker;
   select * from worker order by salary asc;
      select * from worker order by salary desc;
SELECT count(*), salary from worker group by salary;
select avg(salary), age from worker group by age;

select * from worker order by salary desc;

select * from worker order by salary limit 1;
select * from worker order by salary desc limit 1;

select * from worker where salary = (select max(salary) from worker);
select abs(salary - (select avg(salary) from worker)) as diff, name, id from worker;