package students;



import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import xml.ParseXml;

public class StudentsXMLCreator {
	
	Document doc;

	public static void main(String argv[]) throws ParserConfigurationException {
		new StudentsXMLCreator().create();

	}
	
	public void create() throws ParserConfigurationException {
		try {
			doc = ParseXml.newDocument();
			createContent();
			ParseXml.saveFile(doc, "students.xml");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createContent() {
		Element rootElement = createCars();
		Element supercars = addSupercars( rootElement);
		ParseXml.setAttribute(doc, supercars, "company", "100");
		addCar(supercars, "formula one", "Jakub");
		addCar(supercars, "sports", "Gromek");
	}

	private  void addCar(Element supercars, String type, String name) {
		Element carname = doc.createElement("carname");
		ParseXml.setAttribute(doc, carname, "type", type);
		carname.appendChild(doc.createTextNode(name));
		supercars.appendChild(carname);
	}

	private  Element addSupercars(Element rootElement) {
		Element supercar = doc.createElement("supercars");
		rootElement.appendChild(supercar);
		return supercar;
	}

	private  Element createCars() {
		Element rootElement = doc.createElement("student");
		doc.appendChild(rootElement);
		return rootElement;
	}
}