package students;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class University {
	
	private Map<Integer, Student> students = new HashMap<>(); 
	
	public University(){}
	public University(List<Student> studentsList) {
		for (Student student : studentsList){
			students.put(student.getNumerIndeksu(), student);
		}
	}
	
	public void addStudent(int numerIndeksu, String imie, String nazwisko) {
		Student student = new Student(numerIndeksu, imie, nazwisko);
		students.put(student.getNumerIndeksu(), student);
		
	}
	
	public boolean studentExist(int numerIndeksu) {
		// delegacja
		return students.containsKey(numerIndeksu);
	}
	
	public Student getStudent(int numerIndeksu) {
		return students.get(numerIndeksu);
	}
	
	public int studentsNumber () {
		return students.size();
	}
	
	public void showAll (){
		for (Student s : students.values()) {
			System.out.println(s.getNumerIndeksu()+ " "+ s.getImie()+" "+s.getNazwisko());
		}
	}
	
	public List<Student> getStudentsList(){
		List<Student> sList = new LinkedList<>();
		for (Student s : students.values()) {
			sList.add(s);
		}
		return sList;
		//return new LinkedList<>(students.values());
	}
	
	public static void main(String[] args) {
		
		List<Student> students = new ArrayList<>();
		students.add(new Student(1234, "Jan", "Kowalski"));
		students.add(new Student(3333, "Anna", "Nowak"));
		students.add(new Student(2121, "Jakub", "Gromek"));
		University u = new University(students);
//		u.addStudent(100, "Jan", "Kowalski");
//		u.addStudent(1001, "Jan1", "Kowalski1");
//		u.addStudent(1002, "Jan2", "Kowalski2");
//		u.addStudent(1003, "Jan3", "Kowalski3");
		
		u.showAll();
		//u.getStudentsList();
		System.out.println(u.getStudentsList());
	}
	
	
	

}
