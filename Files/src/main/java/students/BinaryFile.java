package students;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BinaryFile implements IFile {

	private final static String FILE_NAME = "binary.txt";

	@Override
	public void save(List<Student> studentsList) {
			DataOutputStream out = null;
		try {
			out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(FILE_NAME)));

			for (Student student : studentsList) {
				out.writeInt(student.getNumerIndeksu());
				out.writeUTF(student.getImie());
				out.writeUTF(student.getNazwisko());
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			
			if(out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public List<Student> load() {
		List<Student> students = new ArrayList<>();
		try (DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(FILE_NAME)))){
			
			
			while(in.available() > 0){
				int numerIndeksu = in.readInt();
				String imie = in.readUTF();
				String nazwisko = in.readUTF();
				Student student = new Student(numerIndeksu, imie, nazwisko);
				students.add(student);
				
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
		return null;
	}

}
