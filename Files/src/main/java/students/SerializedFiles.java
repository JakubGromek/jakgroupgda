package students;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

public class SerializedFiles implements IFile {
	
	private final static String FILE_NAME = "serialized.txt";

	@Override
	public void save(List<Student> studentsList) {
		
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(FILE_NAME))) {
			out.writeObject(studentsList);
		} catch(FileNotFoundException e){
			e.printStackTrace();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Student> load() {
		try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(FILE_NAME))){
			@SuppressWarnings("unchecked")
			List<Student> students = (List<Student>)  in.readObject();
			return students;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	

}
