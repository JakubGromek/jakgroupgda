package students;

import static xml.ParseXml.getText;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import xml.ParseXml;

public class XMLFile implements IFile {

	private final static String FILE_NAME = "students_xml.xml";

	@Override
	public void save(List<Student> studentsList) {

		try {
			Document doc = ParseXml.newDocument();
			createContent(doc, studentsList);
			ParseXml.saveFile(doc, FILE_NAME);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createContent(Document doc, List<Student> studentsList) {
		Element rootElement = doc.createElement("students");
		doc.appendChild(rootElement);

		for (Student s : studentsList) {
			addStudent(doc, rootElement, s);
		}
	}

	public void addStudent(Document doc, Element rootElement, Student s) {

		Element student = doc.createElement("student");
		Attr attr = doc.createAttribute("index");
		attr.setValue(s.getNumerIndeksu() + "");
		student.setAttributeNode(attr);

		Element Imie = doc.createElement("Imie");
		Imie.appendChild(doc.createTextNode(s.getImie()));
		student.appendChild(Imie);

		Element nazwisko = doc.createElement("Nazwisko");
		nazwisko.appendChild(doc.createTextNode(s.getNazwisko()));
		student.appendChild(nazwisko);
		rootElement.appendChild(student);

	}

	@Override
	public List<Student> load() {
		Document doc;
		List<Student> studentsList = new ArrayList<>();
		try {
			doc = ParseXml.parseFile(FILE_NAME);
			NodeList students= doc.getElementsByTagName("student");
			for (int i=0; i<students.getLength(); i++){
			   Node studentNode = students.item(i);
			   Student s = parse(studentNode);
			   studentsList.add(s);
		}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
		
		return studentsList;
	}

	private Student parse(Node studentNode) {
		
//		if (studentNode.getNodeType() == Node.ELEMENT_NODE) {
//			Element student = (Element) studentNode;
//			System.out.print(student.getAttribute("index") );
//			System.out.print(" : " +getText(student, "Imie"));
//			System.out.println(" " +getText(student, "Nazwisko") );
//
//		
//		}
		Element student = (Element) studentNode;
		//System.out.print(student.getAttribute("index") );
		int index = Integer.parseInt(student.getAttribute("index"));
		String name = ParseXml.getText(student, "Imie");
		String surname = ParseXml.getText(student, "Nazwisko");
		return new Student (index,name, surname);
	}

}
