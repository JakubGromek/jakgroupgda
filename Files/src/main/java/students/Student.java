package students;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Student implements Serializable{
	private  int numerIndeksu;
	private  String imie;
	private  String nazwisko;
	
	public Student(){
		
	}

	public void setNumerIndeksu(int numerIndeksu) {
		this.numerIndeksu = numerIndeksu;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public Student(int numerIndeksu, String imie, String nazwisko) {
		this.numerIndeksu = numerIndeksu;
		this.imie = imie;
		this.nazwisko = nazwisko;

	}

	@Override
	public String toString() {
		return " Student, NumerIndeksu=" + numerIndeksu + ", Imie: " + imie + ", Nazwisko: " + nazwisko + "\r";
	}

	public int getNumerIndeksu() {
		return numerIndeksu;
	}

	public String getImie() {
		return imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}
	
	

}
