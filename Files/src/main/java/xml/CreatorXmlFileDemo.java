package xml;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CreatorXmlFileDemo {
	
	Document doc;

	public static void main(String argv[]) throws ParserConfigurationException {
		new CreatorXmlFileDemo().create();

	}
	
	public void create() throws ParserConfigurationException {
		try {
			doc = ParseXml.newDocument();
			createContent();
			ParseXml.saveFile(doc, "cars.xml");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createContent() {
		Element rootElement = createCars();
		Element supercars = addSupercars( rootElement);
		ParseXml.setAttribute(doc, supercars, "company", "Ferrari");
		addCar(supercars, "formula one", "Ferrari 101");
		addCar(supercars, "sports", "Ferrari 102");
	}

	private  void addCar(Element supercars, String type, String name) {
		Element carname = doc.createElement("carname");
		ParseXml.setAttribute(doc, carname, "type", type);
		carname.appendChild(doc.createTextNode(name));
		supercars.appendChild(carname);
	}

	private  Element addSupercars(Element rootElement) {
		Element supercar = doc.createElement("supercars");
		rootElement.appendChild(supercar);
		return supercar;
	}

	private  Element createCars() {
		Element rootElement = doc.createElement("cars");
		doc.appendChild(rootElement);
		return rootElement;
	}
}