package pl.org.pfig.decoratorPerson;

import pl.org.pfig.decoratorSportsman.WaterDrinking;

import java.io.PrintStream;

/**
 * Created by RENT on 2017-06-23.
 */
public class WeightPrinter implements PersonPrinter {
    private final PersonPrinter personPrinter;

    public WeightPrinter(PersonPrinter personPrinter){
        this.personPrinter = personPrinter;
    }

    @Override
    public void print(Person person, PrintStream printStream) {
        personPrinter.print(person, printStream);
      printStream.println("weight: " + person.getWeight());
    }
}
