package pl.org.pfig.decoratorPerson;

import java.io.PrintStream;

/**
 * Created by RENT on 2017-06-23.
 */
public class BasicDataPrinter implements PersonPrinter {




    @Override
    public void print(Person person, PrintStream printStream) {
        printStream.println("name : " + person.getName());
        printStream.println("surname : " + person.getSurname());
    }
}
