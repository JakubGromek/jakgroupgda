package pl.org.pfig.decoratorPerson;

import java.io.PrintStream;

/**
 * Created by RENT on 2017-06-23.
 */
public class EyesColorPrinter implements PersonPrinter {
    private final PersonPrinter personPrinter;
    public EyesColorPrinter(PersonPrinter personPrinter){
        this.personPrinter = personPrinter;
    }

    @Override
    public void print(Person person, PrintStream printStream) {
        personPrinter.print(person, printStream);
printStream.println("Eyes: " +person.getEyesColor());
    }
}
