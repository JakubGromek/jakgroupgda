package pl.org.pfig.decoratorPerson;

/**
 * Created by RENT on 2017-06-23.
 */
public class ConsoleTest {
    public static void main(String[] args) {
        PersonPrinter personPrinter = new EyesColorPrinter(new HeightPrinter(new WeightPrinter(new AgePrinter(new BasicDataPrinter()))));

        Person person = new PersonBuilder()
                .withName("Jan")
                .withSurname("Kowalski")
                .withAge(23)
                .withWeight(77)
                .withHeight(1.88)
                .withEyesColor(Person.EyesColor.BLUE)
                .createPerson();
        personPrinter.print(person, System.out);

        FilePrinter fp = new FilePrinter(personPrinter);

		fp.printToFile(person, "person.txt");


    }
}
