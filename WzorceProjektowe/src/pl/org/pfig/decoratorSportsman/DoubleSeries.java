package pl.org.pfig.decoratorSportsman;

/**
 * Created by RENT on 2017-06-23.
 */
public class DoubleSeries implements Sportsman {
    private Sportsman sportsman;
    public DoubleSeries (Sportsman sportsman) {
        this.sportsman = sportsman;
    }
    @Override
    public void prepare() {
        System.out.println("Robię podwójną rozgrzewkę");
        sportsman.prepare();
        sportsman.prepare();
    }

    @Override
    public void doPumps(int number) {
        System.out.println("Robię podwójną serię pompek");
        sportsman.doPumps(number );
        sportsman.doPumps(number );
    }

    @Override
    public void doSquats(int number) {
        System.out.println("Robię podwójną serię przysiadów");
        sportsman.doSquats(number );
        sportsman.doSquats(number );

    }

    @Override
    public void doCrunches(int number) {
        System.out.println("Robię podwójną serię brzuszków");
        sportsman.doCrunches(number );
        sportsman.doCrunches(number );

    }
}
