package pl.org.pfig.decoratorSportsman;

/**
 * Created by RENT on 2017-06-23.
 */
public class FitnessStudio {

    public void train(Sportsman sportsman){
        sportsman.prepare();
        sportsman.doPumps(8);
        sportsman.doSquats(8);
        sportsman.doCrunches(6);

    }


}
