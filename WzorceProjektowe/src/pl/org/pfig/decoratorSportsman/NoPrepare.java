package pl.org.pfig.decoratorSportsman;

/**
 * Created by RENT on 2017-06-23.
 */
public class NoPrepare implements Sportsman {
    @Override
    public void prepare() {

    }

    @Override
    public void doPumps(int number) {

    }

    @Override
    public void doSquats(int number) {

    }

    @Override
    public void doCrunches(int number) {

    }
}
