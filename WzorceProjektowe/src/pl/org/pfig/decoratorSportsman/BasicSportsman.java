package pl.org.pfig.decoratorSportsman;

/**
 * Created by RENT on 2017-06-23.
 */
public class BasicSportsman implements Sportsman {
    @Override
    public void prepare() {
        System.out.println("Rozgrzewka..");
    }

    @Override
    public void doPumps(int pumps) {

        for (int i=1; i<=pumps; i++) {
            System.out.println("Pompka: " + (i));
        }

    }

    @Override
    public void doSquats(int squats) {
        for (int i=1; i<=squats; i++) {
            System.out.println("Robię przysiad: "+ i);
        }
    }

    @Override
    public void doCrunches(int crunches) {
        for (int i=1; i<=crunches; i++) {
            System.out.println("Robię brzuszek nr: "+ i);
        }
    }
}
