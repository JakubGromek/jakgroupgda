package oop;

public class Osoba {
	//stan
	private String imie;
	private int wiek;
	//konstruktor
	public Osoba(String imie, int wiek) {
	this.imie = imie;
	this.wiek = wiek;
	}
	//zachowanie
	public void przedstawSie(){
		System.out.println(this.imie + " (" + this.wiek +")"  );
	}
	
	
	public String getImie() {
		return imie;
	}
	public int getWiek() {
		return wiek;
	}
	public void setWiek(int wiek) {
		this.wiek = wiek;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}

}
