package QuadraticEquation;

public class QuadraticEquationMain {
	
	public static void main(String[] args) {
		
		quadraticEquation equation = new quadraticEquation(1,4,3);
		
		System.out.println(equation.calcDelta());
		System.out.println("X1= " + equation.calcX1());
		System.out.println("X2= " + equation.calcX2());
		
	}

}
