package mutable.types;

import org.junit.Test;

public class MutableDoubleTest {

	@Test
	public void test() {
		MutableDouble a = new MutableDouble(2.1);
		MutableDouble b = a;

		a.setValue(10.1);

		assert a.getValue() == 10.1;
		assert b.getValue() == 10.1;

		a.increment();

		assert a.getValue() == 11.1;
		assert b.getValue() == 11.1;
	}

}
