/**
 * Created by RENT on 2017-06-29.
 */
public class ScreenRecorder {

//    ScreenRecorder - klasa którą zarządzamy.
//            Profile - klasa którą ustawiamy/tworzymy.
//    ScreenRecorder powinien posiadać listę profili, oraz aktualnie ustawiony profil (pole w klasie).
//    Profile - klasa którą tworzymy i dodajemy.
//    ScreenRecorder powinien mieć metodę
//- addProfile(Profile nowyProfil) - dodaje profil do listy
//- setProfile(int numerProfilu) - ustawia profil (pole) na wybrany z listy
//- listProfiles() - wylistowuje profile
//- startRecording() - rozpoczyna nagrywanie (tylko gdy jeszcze nie nagrywamy)
//- stopRecording() - zatrzymuje nagrywanie (tylko gdy już nagrywamy)


    public void addProfile(Profile nowyProfil) {

        Profile p1 = new Profile(Codec.h263, Extension.AVI, Resolution.R1980x1080);
        Profile p2 = new Profile(Codec.H264, Extension.MKV, Resolution.R1680x1050);

        Profile p3 = new Profile(Codec.THEORA, Extension.MP4, Resolution.R1024x780);


    }

    public int setProfile(int numerProfilu) {
        return 0;
    }

    public String listProfiles() {
        return null;
    }

    public void startRecording() {

    }

    public void stopRecording() {

    }

}
