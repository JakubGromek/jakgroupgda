package queue;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by RENT on 2017-06-27.
 */
public class Main {
    public static void main(String[] args) {

        // SimpleQueueExercise queue = new SimpleQueueExercise();
        SimpleLinkedQueue queue = new SimpleLinkedQueue();

        queue.offer(2);
        queue.offer(5);
        queue.offer(3);
        queue.offer(9);
        queue.offer(11);


        System.out.println(queue.peek());
        System.out.println(queue.isEmpty());
        System.out.println(queue.poll());
        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println(queue.peek());
        System.out.println(queue.poll());

        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println(queue.isEmpty());
    }
}
