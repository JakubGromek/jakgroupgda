package queue;

import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */
public class SimpleQueueExercise implements SimpleQueue {
    private int[] data = new int[4];
    private int start = 0;
    private int end = 0;
    private boolean isEmpty = true;

    @Override
    public boolean isEmpty() {

        return isEmpty;
    }

    @Override
    public void offer(int value) {
        if (end == start && !isEmpty) {
            int[] newData = new int[data.length * 2];
            int i = 0;
            while (!isEmpty) {
                newData[i] = poll();
                i++;
            }
            data = newData;
            start = 0;
            end = i;
        }
        data[end] = value;
        end = (end + 1) % data.length;
        isEmpty = false;

    }

    @Override
    public int poll() {
        if (!isEmpty) {
            int result = data[start];
            start = (start + 1) % data.length;
            isEmpty = (start == end);
            return result;
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public int peek() {
        if (!isEmpty) {
            return data[start];
        }
        throw new ArrayIndexOutOfBoundsException("Kolejka jest pusta");
    }
}
