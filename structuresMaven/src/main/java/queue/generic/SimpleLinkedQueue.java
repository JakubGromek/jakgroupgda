package queue.generic;

/**
 * Created by RENT on 2017-06-27.
 */
public class SimpleLinkedQueue<T> implements SimpleQueue<T> {
    // private int[] data = new int[4];
    private Element<T> first;
    private Element<T> last;
    private int number;

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public void offer(T value) {
        Element element = new Element();
        element.value = value;
        if (isEmpty()) {
            first = element;
            last = element;
        } else {
            last.next = element;
        }
        last = element;
    }

    @Override
    public T poll() {
        T result = peek();
        first = first.next;
        if (first == null) {
            last = null;
        }
        return result;
    }

    @Override
    public T peek() {

        if (!isEmpty()) {
            return (T) first.value;
        }
        throw new ArrayIndexOutOfBoundsException("Kolejka jest pusta");

    }

    private static class Element<T> {
        T value;
        Element next;
        Element prev;

        public Element() {
            this.next = next;
            this.prev = prev;
        }
    }
}


