package queue;

/**
 * Created by RENT on 2017-06-27.
 */
public class SimpleLinkedQueue implements SimpleQueue {
    // private int[] data = new int[4];
    private Element first;
    private Element last;
    private int number;

    @Override
    public boolean isEmpty() {
        return first == null;
    }

    @Override
    public void offer(int value) {
        Element element = new Element();
        element.value = value;
        if (isEmpty()) {
            first = element;
            last = element;
        } else {
            last.next = element;
        }
        last = element;
    }

    @Override
    public int poll() {
        Integer result = peek();
        first = first.next;
        if (first == null){
          last = null;
        }
        return result;
    }

    @Override
    public int peek() {

        if (!isEmpty()) {
            return first.value;
        }
        throw new ArrayIndexOutOfBoundsException("Kolejka jest pusta");

    }

    private static class Element {
        int value;
        Element next;
        Element prev;

        public Element() {
            this.next = next;
            this.prev = prev;
        }
    }
}


