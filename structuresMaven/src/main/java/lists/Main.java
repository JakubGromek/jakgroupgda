package lists;

import java.util.LinkedList;

/**
 * Created by RENT on 2017-06-23.
 */
public class Main {

    public static void main(String[] args) {

//        SimpleList simpleList = new SimpleArrayList();
//
//        simpleList.add(1);
//        simpleList.add(3);
//        simpleList.add(5);
//        simpleList.add(4, 3);
//        simpleList.add(5, 4);
//        simpleList.add(6, 5);
//
//        System.out.println(simpleList.get(1));
//        System.out.println(simpleList.get(0));
//        System.out.println(simpleList.get(2));
//        System.out.println("Rozmiar: ");
//        System.out.println(simpleList.size());
//
//        System.out.println(simpleList.get(3));
//        System.out.println(simpleList.get(4));
//        System.out.println(simpleList.get(5));
//
//        simpleList.removeValue(5);
//
//        System.out.println(simpleList.contain(5));

        SimpleLinkedList linkedList = new SimpleLinkedList();
        linkedList.add(4);
        linkedList.add(6);
        linkedList.add(8);
        //linkedList.add(9,3);

        System.out.println(linkedList.get(0));
        System.out.println(linkedList.get(1));
        System.out.println(linkedList.get(2));
        System.out.println("Rozmiar: ");
        System.out.println(linkedList.size());
        //System.out.println("Ostatni indeks: ");
//        System.out.println(linkedList.getLast());
//        System.out.println(linkedList.indexOf(8));
        linkedList.remove(0);
        System.out.println(linkedList.contain(4));
        System.out.println("Rozmiar: ");
        System.out.println(linkedList.size());
        System.out.println(linkedList.get(0));
        System.out.println(linkedList.get(1));
        System.out.println(linkedList.get(2));


    }
}
