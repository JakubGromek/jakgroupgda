package sort;

/**
 * Created by RENT on 2017-06-27.
 */
public class SortExample {

    public static void main(String[] args) {


        int[] tab = {8, 4, 5, 1, 6, 22, 6, 45, 15, 12, 556};

        sortExample(tab);
    }

    public static void sortExample(int[] tab) {
        int count = 1;
        while (count > 0) {
            count = 0;
            for (int i = 1; i < tab.length; i++) {
                for (int j = i; j < tab.length; j++) {
                    int left = tab[i - 1];
                    int right = tab[i];
                    if (left > right) {
                        tab[i - 1] = right;
                        tab[i] = left;
                        count++;
                    }
                }
            }
        }
        for (int t : tab) {
            System.out.println(t);
        }
    }

    public static void bubbleSort(int[] tab) {

        for (int i = 0; i < tab.length; i++) {
            for (int j = tab.length - 1; j > i; j--) {
                int left = tab[j - 1];
                int right = tab[j];
                if (left > right) {
                    tab[j - 1] = right;
                    tab[j] = left;
                }
            }
        }
//        for (int t : tab) {
//            System.out.println(t);
//        }
    }
}







