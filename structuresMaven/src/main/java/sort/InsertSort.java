package sort;

/**
 * Created by RENT on 2017-06-28.
 */
public class InsertSort {

    public static void insertSort(int[] data) {
        for (int i=0; i<data.length; i++){
            int currentElement = data[i];
            int position = 0;
            while (data[position] < currentElement && position <= i){
                position++;
            }
           for (int j = i; j > position; j--){
                data[j] = data[j-1];
           }
           data[position] = currentElement;
        }
    }
}
