package sort;

/**
 * Created by RENT on 2017-06-28.
 */
public class SelectionSort {

    public static void selectionSort(int[] data) {
        for (int i = 0; i < data.length; i++) {

            int min = data[i];
            int minIndex = i;
            for (int j = i + 1; j < data.length; j++) {
                if (data[j] < min) {
                    min = data[j];
                    minIndex = j;
                }
            }


            int buffer = data[i];
            data[i] = min;
            data[minIndex] = buffer;

        }

    }
}