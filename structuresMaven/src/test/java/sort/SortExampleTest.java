package sort;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by RENT on 2017-06-28.
 */
public class SortExampleTest {

    interface SortMethod {
        void sort(int[] data);
    }

    private int[] generateData(int size) {
        Random random = new Random();
        int[] data = new int[size];
        for (int i = 0; i < size; i++) {
            data[i] = random.nextInt();
        }
        return data;
    }

    public void testSort(SortMethod sortMethod) {
        int[] data = {1, 7, 2, 4, 3, 8, 9, 6};
        sortMethod.sort(data);
        Assertions.assertThat(data).isSorted();


    }

    public void speedTest(SortMethod sortMethod, int[] data) {
        System.out.println("Test of " + data.length + " elements.");
        long start = System.currentTimeMillis();
        sortMethod.sort(data);
        long end = System.currentTimeMillis();
        long time = end - start;
        System.out.println(time / 1000 + "," + time % 1000 + " sec.");
        Assertions.assertThat(data).isSorted();
    }

    public void speedTest(SortMethod sortMethod, String name) {
        System.out.println("Test method " + name);
        speedTest(sortMethod, generateData(1000));
        speedTest(sortMethod, generateData(2 * 1000));
    }


    @Test
    public void sort() throws Exception {
        //  int[] data = {1, 7, 2, 4, 3, 8, 9, 6};
        //int[] data1 = {1, 2, 3, 4};


        // Assertions.assertThat(data).isSorted();
        testSort(SortExample::sortExample);
    }

    @Test
    public void insertSortTest() throws Exception {

        testSort(InsertSort::insertSort);
    }

    @Test
    public void boubbleSortTest() throws Exception {
        testSort(SortExample::bubbleSort);
    }

    @Test
    public void insertSortSpeedTest() throws Exception {
        speedTest(InsertSort::insertSort, "insertSort");
    }
    @Test
    public void sortExampleSpeedTest() throws Exception {
        speedTest(SortExample::sortExample, "sortExample");
    }

    @Test
    public void mergeSortTest() throws Exception {
        int[] left = {1,4,8};
        int[] right = {2,3,5};
                int[] result = new int[6];
                MergeSort.merge(left, right, result);
                Assertions.assertThat(result).isSorted();
            }

    @Test
    public void copyTest() {
        int[] data = {1,22,12,31};
        int[] result = MergeSort.copy(1, 2, data);
        Assertions.assertThat(result).containsExactly(22,12);
    }
}