package pl.org.pfig.main;

import java.util.Random;

public class Main {

	public static void main(String[] args) {
		String myStr = "  przyk�adowy ciag znakow  ";
		
		if (myStr.equals("  przyk�adowy ciag znakow  ")) {
			System.out.println("Ciagi sa takie same.");
		}
		
		if(myStr.equalsIgnoreCase("  przyk�adowy ciag ZNAKOW  ")) {
			System.out.println("Ciagi znakow sa takie same bez badania wielkosci liter.");
		}
		
		System.out.println("dlugosc my.Str to: " +myStr.length());		
		System.out.println(myStr.substring(14));
		
		String otherStr = "Kuba";		
		System.out.println(otherStr.substring(1)); // ucina ciag znakow od wyznaczonego miejsca
		
		System.out.println(otherStr.substring(1, 3)); //ucina od do
		System.out.println(otherStr.substring(1, otherStr.length()-1));
		
		System.out.println(myStr.trim()); //kasuje spacje (puste miejsca)
		
		//myStr = myStr.trim();
		System.out.println(myStr.charAt(2));
		System.out.println(myStr.charAt(2) + "" + myStr.charAt(3));
		
		String alphabet = "";
		for(char c = 97; c <= 122; c++) {   // wpisuje do ciagu znaki z tablicy ASCII
			alphabet += c;
		}
		System.out.println(alphabet);
		
		System.out.println(myStr.replaceAll("ciag", "lancuch")); //podmienia wpisane wyrazy
		
		System.out.println(myStr.concat(otherStr)); // �aczy ciagi znakow
		
		System.out.println(myStr);
		
		if (myStr.contains("k�ad")) {
			System.out.println("Slowo klad zawiera sie w " + myStr);
		}
		
		if(alphabet.startsWith("a")) {
			System.out.println("alfabet zaczyna sie od A");
		}
		
		if(alphabet.endsWith("z")) {
			System.out.println("alfabet konczy sie na Z");
		}
		
		
		System.out.println(myStr.indexOf("a")); //pokazuje miejsce litery a
		System.out.println(myStr.lastIndexOf("a")); //pokazuje miejsce ostatniej litery a
		
		String simpleStr = "pawel poszedl do lasu";
		String[] arrOfStr = simpleStr.split(" ");
		for (String s : arrOfStr) {
			System.out.println("\t" +s);
		}
		
		String newS = "Jakis CIaG";
		System.out.println(newS.toLowerCase()); // zmienia na male znaki
		System.out.println(newS.toUpperCase()); // zmienia na wielkie znaki
		
		System.out.println();
		
		
		Random r = new Random();
		// jezeli nie przekazujemy parametru, moze zostac zwrocona liczba ujemna
		System.out.println(r.nextInt());
		// jezeli przekazujemy argument, zostanie zwrocona liczba z zakresu <0, liczba)
		System.out.println(r.nextInt(15));
		// <10, 25) - generowanie losowej liczby z zakresu
		int a = 10;
		int b = 25;
		System.out.println(a + r.nextInt(b-a));
		
		char c = (char) (97 + r.nextInt(26));
		System.out.println(c + "");
		
		System.out.println();
		
		HTMLExercise he = new HTMLExercise("To jest tekst");
		he.print().strong().p().print();
		System.out.println();
		
		StringUtil su = new StringUtil("przykladowy tekst");
		StringUtil su1 = new StringUtil("przykladowy tekst");
		StringUtil su2 = new StringUtil("przykladowy tekst");
		StringUtil su3 = new StringUtil("przykladowy tekst");
		StringUtil su4 = new StringUtil("");
		StringUtil su5 = new StringUtil("przykladowy tekst");
		StringUtil su6 = new StringUtil("przykladowy tekst");
		StringUtil su7 = new StringUtil("przykladowy tekst");
		su.prepend("__").print();
		su1.append("**").print();				
		su2.letterSpacing().print(); 
		su3.reverse().print();
		su4.getAlphabet().print();
		su5.getFirstLetter().print();
		su6.limit(5).print();
		su7.insertAt("-nowe-", 5).print();
		su.resetText().print();
		
		
		// tablica dwuwymiarowa
		int [][] testArray = {{49,2,3},
		                      {4,7},
				              {21,13,9}};
		TwoDArrays td = new TwoDArrays();
		System.out.println("Suma elementow podzielnych przez 7 w tablicy to: " +td.addElementsDividedBySeven(testArray));
		System.out.println("Iloczyn elementow tablicy :" + td.multiplyElements(testArray));
		System.out.println("Iloczyn elementow parzystych: " + td.multiplyEvenElements(testArray));
		System.out.println("Iloczyn elementow nieparzystych: " + td.multiplyOddElements(testArray));

		
	
		
	}

	

}
