package pl.org.pfig.main;

public class StringUtil {
	
	private String str;
	
	public StringUtil(String str) {
		this.str = str;
	}
	
	public StringUtil print() {
		System.out.println(this.str);
		return this;
	}
	
	public StringUtil prepend(String arg){
		str = arg + str;
		return this;
	}
	
	public StringUtil append(String arg) {
		str = str + arg;		
		return this;
	}
	
	public StringUtil letterSpacing() {
		String[] letters = str.split("");
		str = "";
		for(String letter : letters) {
			str += letter + " ";
		}
		str = str.substring(0, str.length() -1);
		
		return this;
	}
	
	public StringUtil reverse() {
		String[] letters = str.split("");				
		str = "";
		for(int i=letters.length-1; i>=0; i--){
			str += letters[i];
		}
		
		
		return this;
	}
	
	public StringUtil getAlphabet() {
		String alphabet = "";
		for(char c = 97; c <= 122; c++) {  
			alphabet += c;
		}
		str = alphabet;
		return this;
	}
	public StringUtil getFirstLetter() {
		str = "" + str.charAt(0);
		return this;
	}
	
	public StringUtil limit(int n){
		str = str.substring(n);
		return this;
	}
	
	public StringUtil insertAt(String st, int n) {
		str =  str.substring(0, n) + st + str.substring(n, str.length());
		
		return this;
	}
	
	public StringUtil resetText() {
		String[] st = new String[5];
		for (String l : st) {
			str = l;
		}
		return this;
	}
	
	public StringUtil swapLetters() {
		//str = str.charAt(str.length()-1) + str.charAt(0);
		return this;
	}

	
	

}
