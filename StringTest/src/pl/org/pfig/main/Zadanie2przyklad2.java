package pl.org.pfig.main;

public class Zadanie2przyklad2 {

	public static void main(String[] args) {
		
		String[] w = Zadanie2przyklad2.checkWordsLength(3, "kiedys wybiore sie do lasu");
		for(String str:w) {
			System.out.println(str);
		}
		
	}
	
	public static String[] checkWordsLength(int n, String s) {
		String[] words = s.split(" ");
		String[] ret = new String[words.length];
		int i = 0;
		for(String word : words) {
			if(word.length() >n) {
				ret[i++] = word;
			}
		}
		return ret;
	}

}