package pl.org.pfig.main;

public class TwoDArrays {

	public int addElementsDividedBySeven(int[][] arr) {
		// deklarujemy
		int sum = 0;
		int rows = arr.length;
		for (int i = 0; i < rows; i++) {
			int cols = arr[i].length;
			for (int j = 0; j < cols; j++) {
				if (arr[i][j] % 7 == 0) {
					sum += arr[i][j];
				}
			}
		}
		return sum;
	}

	public int multiplyElements(int[][] arr) {
		int sum = 1;
		int rows = arr.length;
		for (int i = 0; i < rows; i++) {
			int cols = arr[i].length;
			for (int j = 0; j < cols; j++) {
				sum *= arr[i][j];
			}
		}
		return sum;
	}

	public int multiplyEvenElements(int[][] arr) {
		int sum = 1;
		int rows = arr.length;
		for (int i = 0; i < rows; i++) {
			int cols = arr[i].length;
			for (int j = 0; j < cols; j++) {
				if (arr[i][j] % 2 == 0) {
					sum *= arr[i][j];
				}
			}
		}
		return sum;

	}

	public int multiplyOddElements(int[][] arr) {
		int sum = 1;
		int rows = arr.length;
		for (int i = 0; i < rows; i++) {
			int cols = arr[i].length;
			for (int j = 0; j < cols; j++) {
				if (arr[i][j] % 2 != 0) {
					sum *= arr[i][j];
				}
			}
		}
		return sum;

	}
}
