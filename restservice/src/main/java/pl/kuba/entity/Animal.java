package pl.kuba.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;

/**
 * Created by RENT on 2017-07-25.
 */
@Entity
public class Animal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String description;
    private String image;
    @JsonManagedReference
    @ManyToOne(cascade = CascadeType.ALL)
    private Specie specie;

    public Animal() {
    }

    public Animal(long id, String name, String description, String image, Specie specie) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.specie = specie;
    }

    public Specie getSpecie() {
        return specie;
    }

    public Animal setSpecie(Specie specie) {
        this.specie = specie;
        return this;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Animal setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getImage() {
        return image;
    }

    public Animal setImage(String image) {
        this.image = image;
        return this;
    }

    //    @Override
//    public String toString() {
//        return "Animal{" +
//                "id=" + id +
//                ", name='" + name + '\'' +
//                '}';
//    }
}