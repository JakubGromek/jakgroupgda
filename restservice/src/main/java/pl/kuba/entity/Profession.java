package pl.kuba.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

/**
 * Created by RENT on 2017-07-27.
 */
@Entity
public class Profession {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    @JsonBackReference
    @OneToMany(mappedBy = "profession")
    private List<Staff> staff;

    public Profession(){}

    public Profession(String name, List<Staff> staff) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public Profession setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Profession setName(String name) {
        this.name = name;
        return this;
    }

    public List<Staff> getStaff() {
        return staff;
    }

    public Profession setStaff(List<Staff> staff) {
        this.staff = staff;
        return this;
    }
}
