package pl.kuba;

/**
 * Created by RENT on 2017-07-25.
 */
public class JsonResponse {

    private String message;

    public JsonResponse() {
    }

    public JsonResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public JsonResponse setMessage(String message) {
        this.message = message;
        return this;
    }
}
