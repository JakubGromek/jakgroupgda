package pl.kuba.controller;

/**
 * Created by RENT on 2017-07-25.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.kuba.entity.Animal;
import pl.kuba.entity.Specie;
import pl.kuba.repository.AnimalRepository;
import pl.kuba.repository.SpecieRepository;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

@CrossOrigin
@RestController
@RequestMapping("/animals")
public class AnimalController {
    @Autowired
    private AnimalRepository animalRepository;
    @Autowired
    private SpecieRepository specieRepository;


    @RequestMapping("/")
    public String animal() {
        return "";
    }


    @RequestMapping("/show")
    public List<Animal> listAnimals() {
        return (List<Animal>) animalRepository.findAll();
    }

    @RequestMapping("/add")
    public Animal addAnimal(@RequestParam(name = "name") String name,
                            @RequestParam(name = "description") String desc,
                            @RequestParam(name = "image") String img,
                            @RequestParam(name = "specie") String specie) {
        long specieId = Long.valueOf(specie);
        Specie s = specieRepository.findOne(specieId);
        Animal a = new Animal();
        a.setName(name);
        a.setDescription(desc);
        a.setImage(img);
        a.setSpecie(s);
        return animalRepository.save(a);
    }

    @RequestMapping("/show/{id}")
    public Animal showAnimalByID(@PathVariable("id") String id) {
        long myId = Integer.valueOf(id);
        return animalRepository.findOne(myId);
    }


}