package pl.kuba.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.kuba.entity.Specie;
import pl.kuba.repository.SpecieRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by RENT on 2017-07-26.
 */
@CrossOrigin
@RestController
@RequestMapping("/species")
public class SpeciesController {

    @Autowired
    private SpecieRepository specieRepository;

    @RequestMapping("/add")
    public Specie add(@RequestParam(name = "name") String name,
                      @RequestParam(name = "description") String description) {
        Specie s = new Specie();
        s.setName(name);
        s.setDescription(description);
        return specieRepository.save(s);
    }

    @RequestMapping("/show")
    public List<Specie> showAll() {
        return (List<Specie>) specieRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Specie showById(@PathVariable(name = "id") String id) {
        long myId = Long.valueOf(id);
        return specieRepository.findOne(myId);
    }
}
