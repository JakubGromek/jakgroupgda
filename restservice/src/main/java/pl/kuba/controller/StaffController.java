package pl.kuba.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.kuba.entity.Profession;
import pl.kuba.entity.Staff;
import pl.kuba.repository.ProfessionRepository;
import pl.kuba.repository.StaffRepository;

import java.util.List;

/**
 * Created by RENT on 2017-07-25.
 */
@CrossOrigin
@RestController
@RequestMapping("/staff")
public class StaffController {

    @Autowired
    private StaffRepository staffRepository;
    @Autowired
    private ProfessionRepository professionRepository;


    @RequestMapping("/")
    public String animal() {
        return "";
    }


    @RequestMapping("/show")
    public List<Staff> listStaff() {
        return (List<Staff>) staffRepository.findAll();
    }

    @RequestMapping("/add")
    public Staff addAnimal(@RequestParam(name = "name") String name,
                            @RequestParam(name = "lastname") String lastname,
                            @RequestParam(name = "salary") double salary,
                            @RequestParam(name = "profession") String profession){
        long professionId = Long.valueOf(profession);
        Profession p = professionRepository.findOne(professionId);
        Staff s = new Staff();
        s.setName(name);
        s.setLastname(lastname);
        s.setSalary(salary);
        s.setProfession(p);
        return staffRepository.save(s);
    }

    @RequestMapping("/show/{id}")
    public Staff showAnimalByID(@PathVariable("id") String id) {
        long myId = Integer.valueOf(id);
        return staffRepository.findOne(myId);
    }


}
