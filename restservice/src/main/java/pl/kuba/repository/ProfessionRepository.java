package pl.kuba.repository;

import org.springframework.data.repository.CrudRepository;
import pl.kuba.entity.Profession;

import java.lang.invoke.LambdaConversionException;

/**
 * Created by RENT on 2017-07-27.
 */
public interface ProfessionRepository extends CrudRepository<Profession, Long> {
}
