package pl.kuba.repository;

import org.springframework.data.repository.CrudRepository;
import pl.kuba.entity.Specie;

/**
 * Created by RENT on 2017-07-26.
 */
public interface SpecieRepository extends CrudRepository<Specie, Long> {

}
