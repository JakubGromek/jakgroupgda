package pl.org.pfig;

public class Main {

	public static void main(String[] args) {

		Main.printAdress("Algorytmiczna", 3);
	}

	public static void printAdress(String street, int num) {
		for (int i = 1; i <= num; i += 2) {
			for (int j = 1; j <= 12; j++) {
				if (j <= 6) {
					System.out.println("ul. " + street + " " + i + "A" + " " + j);
				} else {
					System.out.println("ul. " + street + " " + i + "B" + " " + j);
				}
			}
		}
	}

}
