package employee;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-06-19.
 */
public class Company {

    public List<Employee> employees = new LinkedList<>();


    public void filter(FilterInterface fi, TransformInterface t) {
        for (Employee e : employees){
            if (fi.test(e.getName())) {
                System.out.println(t.transform(e.getName()));
            }
        }

    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}

