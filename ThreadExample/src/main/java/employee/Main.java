package employee;

import java.util.ArrayList;
import java.util.List;

public class Main {



    public static void main(String[] args) {

        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Paweł" ,"Testowy" ,18,3000));
        employees.add(new Employee("Piotr","Przykładowy",32,10000));
        employees.add(new Employee("Zofia","Zaspa",37,3700));
        employees.add(new Employee("Julia","Doe",41,4300));
        employees.add(new Employee("Przemysław", "Wietrak", 56, 4500));

        Company c = new Company();
        c.setEmployees(employees);
        c.filter(n -> n.startsWith("P"), t -> t.toUpperCase());

//
        for(Employee emp : employees) {
            if(emp.getName().startsWith("P")) {
                System.out.println(emp);
            }

        }
//        System.out.println();
//        for(Employee emp : employees) {
//            if(emp.getSalary() > 4000) {
//                System.out.println(emp);
//            }
//
//        }
        System.out.println(employees.get(3));


        System.out.println();
//        for(Employee emp : employees) {
//            if(emp) {
//                System.out.println(emp);
//            }
//
//        }

    }
}
