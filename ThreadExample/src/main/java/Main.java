

import java.util.LinkedList;
import java.util.List;


public class Main {
    public static void main(final String[] args) {
        Thread t = new Thread(new PrzykladowyWatek());

        t.start();

        new Thread(new InnyWatek(), "Jakis watek").start();
        System.out.println("KONIEC");

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Pochodze z kodu Runnable");
            }
        }).start();

        new Thread(() -> {
            int a = 1, b = 3;
            System.out.println("Suma wynosi "+ (a+b) );
        }).start();

        new Thread( () -> System.out.println("Wiadomosc  z FI") ).start();

        List<Person> p = new LinkedList();
        p.add(new Person(1));

        Calc c = new Calc();
        //System.out.println(c.add(3,4));

        System.out.println(c.oper(5, 6, new CalculatorInterface(){
            @Override
            public double doOperation(int a, int b) {
                return a+b;
            }
        }));

        System.out.println(c.oper(17,3, ( a, b) -> (a-b)));
        System.out.println(c.oper(17,3, ( a, b) -> (a*b)));

        Animal a = new Animal();
        a.addAnimal("pies" , (anims) -> (anims.split(" ")));
        a.addAnimal("kot|zaba|rys" , (anims) -> (anims.split("\\|")));

        a.addAnimal("katp", (anims) -> {
            String[] ret = new String[1];
            ret[0] = "";
            for (int i = anims.length()-1; i >= 0; i--){
                ret[0]+=anims.charAt(i)+"";
            }
            System.out.println(ret[0]);
            return ret;
        });
        System.out.println(a.containsAnimal("ptak|kot", (s) -> s.split("\\|")));

        System.out.println();
        //System.out.println(a.containsAnimal("rys"));
//        System.out.println(a.addAnimal("pies", new AnimalInterface() {
//            @Override
//            public String[] add(String animal) {
//                return new String[0];
//            }
//        }));
       // System.out.println(a);

    }
}
