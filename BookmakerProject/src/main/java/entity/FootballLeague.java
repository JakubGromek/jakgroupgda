package entity;
import javax.persistence.*;

/**
 * Created by RENT on 2017-08-03.
 */
@Entity
@Table (name = "footballleague")
public class FootballLeague {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "team")
    private String team;
    @Column(name = "draws")
    private int draws;
    @Column (name = "season")
    private String season;

    public FootballLeague(){}

    public FootballLeague(String team, int draws, String season) {
        this.team = team;
        this.draws = draws;
        this.season = season;
    }

    public int getId() {
        return id;
    }

    public String getTeam() {
        return team;
    }

    public int getDraws() {
        return draws;
    }

    public String getSeason() {
        return season;
    }
}
