package menu;

import entity.FootballLeague;
import org.hibernate.Session;
import util.HibernateUtil;

import java.util.List;

/**
 * Created by RENT on 2017-08-04.
 */
public class Option4 {

    public static void showFullTable() {
        Session session = HibernateUtil.openSession();
        List<FootballLeague> footballLeagueList = session.createQuery("from FootballLeague order by team").list();

        for (FootballLeague f : footballLeagueList) {
            System.out.println(f.getTeam() + " - remisowali: " + f.getDraws() + " razy, w sezonie: " + f.getSeason());
        }
        session.close();

    }

    public static void showDatabaseDraws10() {
        Session session = HibernateUtil.openSession();
        List<FootballLeague> footballLeagueList = session.createQuery("from FootballLeague where draws >= 10 order by team").list();

        for (FootballLeague f : footballLeagueList) {
            System.out.println(f.getTeam() + " - remisowali: " + f.getDraws() + " razy, w sezonie: " + f.getSeason());
        }
        session.close();

    }

    public static void showDatabaseDraws12() {
        Session session = HibernateUtil.openSession();
        List<FootballLeague> footballLeagueList = session.createQuery("from FootballLeague where draws >= 12 order by team").list();
        for (FootballLeague f : footballLeagueList) {
            System.out.println(f.getTeam() + " - remisowali: " + f.getDraws() + " razy, w sezonie: " + f.getSeason());
        }
        session.close();

    }

    public static void showDatabaseDraws14() {
        Session session = HibernateUtil.openSession();
        List<FootballLeague> footballLeagueList = session.createQuery("from FootballLeague where draws >= 14 order by team").list();

        for (FootballLeague f : footballLeagueList) {
            System.out.println(f.getTeam() + " - remisowali: " + f.getDraws() + " razy, w sezonie: " + f.getSeason());
        }
        session.close();

    }

}
