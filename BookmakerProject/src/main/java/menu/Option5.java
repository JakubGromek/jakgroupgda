package menu;

import entity.LeagueTwo;
import org.hibernate.Session;
import util.HibernateUtil;

import java.util.List;

/**
 * Created by RENT on 2017-08-08.
 */
public class Option5 {

    public static void showFullTable() {
        Session session = HibernateUtil.openSession();
        List<LeagueTwo> leagueTwoList = session.createQuery("from LeagueTwo order by team").list();

        for (LeagueTwo f : leagueTwoList) {
            System.out.println(f.getTeam() + " - remisowali: " + f.getDraws() + " razy, w sezonie: " + f.getSeason());
        }
        session.close();

    }

    public static void showDatabaseDraws10() {
        Session session = HibernateUtil.openSession();
        List<LeagueTwo> leagueTwoList = session.createQuery("from LeagueTwo where draws >= 10 order by team").list();

        for (LeagueTwo f : leagueTwoList) {
            System.out.println(f.getTeam() + " - remisowali: " + f.getDraws() + " razy, w sezonie: " + f.getSeason());
        }
        session.close();

    }

    public static void showDatabaseDraws12() {
        Session session = HibernateUtil.openSession();
        List<LeagueTwo> leagueTwoList = session.createQuery("from LeagueTwo where draws >= 12 order by team").list();
        for (LeagueTwo f : leagueTwoList) {
            System.out.println(f.getTeam() + " - remisowali: " + f.getDraws() + " razy, w sezonie: " + f.getSeason());
        }
        session.close();

    }

    public static void showDatabaseDraws14() {
        Session session = HibernateUtil.openSession();
        List<LeagueTwo> leagueTwoList = session.createQuery("from LeagueTwo where draws >= 14 order by team").list();

        for (LeagueTwo f : leagueTwoList) {
            System.out.println(f.getTeam() + " - remisowali: " + f.getDraws() + " razy, w sezonie: " + f.getSeason());
        }
        session.close();

    }

}
