package menu;

/**
 * Created by RENT on 2017-07-28.
 */
public class Option2 {

    int stawka, nstawka, zdarzen;
    double kurs, skurs, wygrana, zysk, wydane, ostwydane, kolejki;


    public double progressiveBetSpend(int stawka, int nstawka, double kolejki) {
        wydane = (stawka*kolejki) + (((kolejki/2)*(kolejki-1)) * nstawka);
        return wydane;
    }

    public double lastSpend (int stawka, int nstawka, double kolejki) {
        ostwydane = stawka + (nstawka*(kolejki-1));
        return ostwydane;
    }
    public double sKurs (double kurs, int zdarzen){
        skurs= Math.pow(kurs, zdarzen);
        return skurs;
    }

    public double win () {
        wygrana = ostwydane * skurs;
        return wygrana;
    }

    public double profit () {
        zysk = wygrana - wydane;
        return zysk*0.88;
    }
}
