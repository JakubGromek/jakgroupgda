package crud;

import entity.FootballLeague;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

import java.util.List;

/**
 * Created by RENT on 2017-08-03.
 */
public class SimpleCRUD<T> implements CrudInterface<T> {
    private Class<T> clazz;

    public SimpleCRUD(Class<T> clazz) {
        this.clazz = clazz;
    }

    public T select(int id) {
        Session session = HibernateUtil.openSession();
        T obj = (T) session.get(clazz, id);
        session.close();
        return obj;
    }

    public void insertOrUpdate(T obj) {
        Session session = HibernateUtil.openSession();
        Transaction tx = session.beginTransaction();
        session.saveOrUpdate(obj);
        tx.commit();
        session.close();
    }

    public void delete(Object obj) {

    }

    public List<T> select() {
        List<T> ret;
        Session session = HibernateUtil.openSession();
        Transaction tx = session.beginTransaction();
        System.out.println("\t" + clazz.getSimpleName());
        ret = session.createQuery("from " + clazz.getSimpleName()).list();
        tx.commit();
        session.close();
        return ret;
    }
}