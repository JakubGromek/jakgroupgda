package pl.org.pfig.tdd;

import java.util.Random;

public class Exercises {

	public int absoluteValue(int num) {
		if (num < 0) {
			num = -num;
		}
		return num;
	}

	public String determineDayOfWeek(int day) throws IllegalArgumentException {
		if (day > 7 || day < 1) {
			throw new IllegalArgumentException("Bad input value.");
		}
		String[] days = { "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday" };
		return days[day - 1];
	}

	public String determineMonth(int month) throws IllegalArgumentException {
		if (month > 12 || month < 1) {
			throw new IllegalArgumentException("Bad input value.");
		}
		String[] months = { "styczen", "luty", "marzec", "kwiecien", "maj", "czerwiec", "lipiec", "sieprien",
				"wrzesien", "pazdziernik", "listopad", "grudzien" };
		return months[month - 1];
	}

	public int highestCommonDivided(int a, int b) {

		return 0;
	}

	public int countTime(int h, int m, int s) {

		if (h < 0 || m < 0 || s < 0 || h > 23 || m > 59 || s > 59) {
			throw new IllegalArgumentException("Bad time value.");
		}

		return (h * 360) + (m * 60) + s;
	}
	
	public int[] processNumbers(int a, int b){

		return new int[1];
	}

}
