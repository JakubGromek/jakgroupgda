package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CountTimeTest {

	Exercises e;
	
	@Before
	public void init(){
		e = new Exercises();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenHourIsNegativeExceptionExpected() {
		int[] data = {-3,12,15};
		e.countTime(data[0],data[1],data[2]);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenMinuteIsNegativeExceptionExpected() {
		int[] data = {3,-12,15};
		e.countTime(data[0],data[1],data[2]);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenSecondIsNegativeExceptionExpected() {
		int[] data = {3,12,-15};
		e.countTime(data[0],data[1],data[2]);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenHourIsOutOfRangExceptionExpected() {
		int[] data = {33,12,15};
		e.countTime(data[0],data[1],data[2]);
	}
	@Test(expected=IllegalArgumentException.class)
	public void whenMinuteIsOutOfRangeExceptionExpected() {
		int[] data = {3,66,15};
		e.countTime(data[0],data[1],data[2]);
	}
	@Test(expected=IllegalArgumentException.class)
	public void whenSecondIsOutOfRangeExceptionExpected() {
		int[] data = {3,12,333};
		e.countTime(data[0],data[1],data[2]);
	}
	
	@Test
	public void whenHourIsLegalHourInSecondExpected(){
		int arg = 2;
		int expected = 720;
		assertEquals(expected, e.countTime(arg, 0, 0));
	}
	
	@Test
	public void whenTimeIsLegalTimeInSecondsEcpected(){
		int arg=1, arg1=1, arg2=6;
		int expected= 426;
		assertEquals(expected, e.countTime(arg, arg1, arg2));	
	}
	
	@Test
	public void test(){
		int arg = 33;
		int expected = 33;
		assertEquals(expected, e.countTime(0,0,arg));
	}
	
	@Test
	public void whenMaxTimeIsGivenProperTimeInSecondsExpected(){
		int[] data = {23,59,59};
		int expected = 11879;
		assertEquals(expected, e.countTime(data[0], data[1], data[2]));
		
	}
	

}
