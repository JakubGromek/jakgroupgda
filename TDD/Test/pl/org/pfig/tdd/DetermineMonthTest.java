package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DetermineMonthTest {
	
	Exercises e;
	@Before
	public void init(){
		e = new Exercises();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenOutOfRangePositiveArgumentExceptionExpected() {	
		int data = 333;
		e.determineMonth(data);
	}
	

	@Test
	public void whenProperValueIsGivenProperMonthIsExpected() {
		int[] data = {1,12};
		String[] expected = {"styczen", "grudzien"};
		
		for (int i=0; i<data.length; i++){
			assertEquals(expected[i], e.determineMonth(data[i]));
		}
	}
	
	@Test
	public void when6IsGivenCzerwiecIsExpected() {
		//int arg = 6;
		String expected = "czerwiec";
		assertEquals(expected, e.determineMonth(6));
	}

}
