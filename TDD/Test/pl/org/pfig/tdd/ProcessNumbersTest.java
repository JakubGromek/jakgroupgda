package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ProcessNumbersTest {
	
	Exercises e;
	
	@Before
	public void init(){
		e = new Exercises();
	}

	@Test
	public void whenArgumentAOutOfRangeExceptionExcepted() {
	IllegalArgumentException ex = null;
		int[] arrayOfa = {-1,256,1000,-1000};
		int b = 8;
		for(int a : arrayOfa){
			try{
			e.processNumbers(a, b);
			}catch(IllegalArgumentException e) {
				ex = e;
			}
			assertTrue(ex != null);
			ex = null;
		}
	}

	@Test(expected=IllegalArgumentException.class)
	public void whenArgumentBOutOfRangeExceptionExcepted() {
		IllegalArgumentException ex =null;
		int a = 3;
		int[] arrayOfb = {-123, 277, -22, 1311};
		for(int b : arrayOfb){
			try{
				e.processNumbers(a, b);
			}catch (IllegalArgumentException e){
				ex = e;
			}
			assertNotNull("Wyjatek nie wystapil", ex);
			ex = null;
		}
	}

	@Test(expected=IllegalArgumentException.class)
	public void whenArgumentsAreIncorrectOrderExceptionExcepted() {
		int a = 8;
		int b = 3;
		e.processNumbers(a, b);
	}

	@Test
	public void whenArgumentsAreProperExpectArrayResult(){
		int a = 3, b = 8;
		int[] expected = {4,6,8,7,5,3};
		assertArrayEquals(expected, e.processNumbers(a, b));
	}
	
	@Test
	public void whenArgumentsAreTheSameExpectOneItemArray(){
		int arg = 5;
		int[] expected = {5};
		assertArrayEquals(expected, e.processNumbers(arg, arg));
				
	}
}
