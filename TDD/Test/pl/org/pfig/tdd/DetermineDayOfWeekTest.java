package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DetermineDayOfWeekTest {
	
	Exercises e;
	
	@Before
	public void init(){
		e = new Exercises();
	}
	@Test
	public void whenOutOfRangeArgumentIsGivenExceptionExpected(){
		int data = -14;
		IllegalArgumentException iae = null;
		try {
			e.determineDayOfWeek(data);
		} catch (IllegalArgumentException e){
			iae = e;
		}
		assertNotNull("Wyjatek nie wystapil", iae);
		
	}
	@Test(expected=IllegalArgumentException.class)
	public void whenOutOfRangePositiveArgumentIsGivenExceptionExpected(){
		int data = 123;
		e.determineDayOfWeek(data);
	}
	
	@Test
	public void when1IsGivenSundayIsExpected() {
		int arg = 1;
		String expected = "sunday";
		assertEquals("sunday", e.determineDayOfWeek(1));
	}
	
	@Test
	public void when2IsGivenMondayIsExpected() {
		int arg = 2;
		String expected = "monday";
		assertEquals("monday", e.determineDayOfWeek(2));
	}
	
	@Test
	public void when3IsGivenTuesdayIsExpected() {
		int arg = 3;
		String expected = "tuesday";
		assertEquals("tuesday", e.determineDayOfWeek(3));
	}
	
	@Test
	public void when4IsGivenWednesdayIsExpected() {
		int arg = 4;
		String expected = "wednesday";
		assertEquals("wednesday", e.determineDayOfWeek(4));
	}
	
	@Test
	public void when5IsGivenThursdayIsExpected() {
		int arg = 5;
		String expected = "thursday";
		assertEquals("thursday", e.determineDayOfWeek(5));
	}
	
	@Test
	public void when6IsGivenFridayIsExpected() {
		int arg = 6;
		String expected = "friday";
		assertEquals("friday", e.determineDayOfWeek(6));
	}
	
	@Test
	public void when7IsGivenSaturdayIsExpected() {
		int arg = 7;
		String expected = "saturday";
		assertEquals("saturday", e.determineDayOfWeek(7));
	}
	
	@Test
	public void whenProperValueIsGivenProperDayResultExpected(){
		int[] data = {1,7};
		String[] expected = {"sunday", "saturday"};
		
		for(int i=0; i<data.length; i++){
			assertEquals(expected[i], e.determineDayOfWeek(data[i]));
		}
		
	}

}
