package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class HighestCommonDividedTest {

	Exercises e;
	
	@Before
	public void init(){
		e = new Exercises();
	}
	
	@Test
	public void whenGivenRandomDataExpectProperResult() {
		int[][] dataSet = {{48,12,12},{210,6,6},{-9,3,3},{21,-7,7}};
		for(int[] data : dataSet) {
			assertEquals(data[2], e.highestCommonDivided(data[0], data[1]));
		}
	}

}
