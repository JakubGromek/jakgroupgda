<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="header.jsp"/>


<div id="content">

<p>Name: <c:out value="${name}"/></p>

    <ul>
    <c:forEach items="${colors}" var="color">
        <li><c:out value="${color}"/></li>
    </c:forEach>
    </ul>


<%--<c:forEach var="i" begin="1" end="5">--%>
    <%--<p> Numerek: <c:out value="${i}"/></p>--%>
<%--</c:forEach>--%>
<table border="1" cellspacing="2" cellpadding="2" width="100%">
    <thead>
    <tr>
        <th>ID</th>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>Płaca</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="i" begin="1" end="10">
        <tr>
            <td><c:out value="${i}"/></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</div>


<c:import url="footer.jsp"/>